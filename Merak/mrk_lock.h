﻿#pragma once

#include "mrk_helper.h"

namespace Merak { namespace lock
{
	class Exclusion
		: private Merak::helper::Noncopyable
	{
	public:
		virtual void lock() = 0;
		virtual void unlock() = 0;
	};

	class SpinWait
		: public Exclusion
	{
	public:
		explicit SpinWait( bool withoutThreadDispatch = false )
			: withoutThreadDispatch_( withoutThreadDispatch ) {}

	private:
		void lock();
		void unlock();

	private:
		struct Impl
		{
			Impl() : initval( 0 ), currentval( initval ), newval( 0xCAFEBABE ) {}

			const long initval;
			volatile long currentval;
			long newval;
		}
		impl_;
		
#if ( 0 < MERAK_SPINLOCK_TIMEOUT )
		static const uint32_t kTimeout_ = MERAK_SPINLOCK_TIMEOUT;
#else
		static const uint32_t kTimeout_ = 0;
#endif
		bool withoutThreadDispatch_;
	};

	class ScopedLock
	{
	public:
		explicit ScopedLock( __in Exclusion & exobj ) : exobj_( exobj )
		{
			exobj_.lock();
		}

		~ScopedLock()
		{
			exobj_.unlock();
		}

	private:
		Exclusion & exobj_;
	};

} } // namespace Merak::lock