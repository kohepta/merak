﻿#pragma once

#include "mrk_substitute.h"

namespace Merak { namespace atomic
{
	inline int CompareAndExchange(
		volatile long & val, 
		long oldval, 
		long newval )
	{
		return Merak::substitute::_InterlockedCompareExchange(
			reinterpret_cast< volatile long * >( &val ), newval, oldval );
	}

	template < typename T >
	inline void * CompareAndExchange(
		T * volatile & val, 
		T * oldval, 
		T * newval )
	{
		void * volatile * ptr = reinterpret_cast< void * volatile * >( &val );
	
		return Merak::substitute::_InterlockedCompareExchangePointer( ptr, newval, oldval );
	}

	template < typename T >
	inline bool AtomicallySetImpl(
		volatile T & val, 
		T newval )
	{
		const T oldval = val;

		return CompareAndExchange( val, oldval, newval ) == oldval;
	}

	template < typename T >
	inline void AtomicallySet(
		volatile T & val, 
		T newval ) 
	{
		while ( !AtomicallySetImpl( val, newval ) )
		{
			continue;
		}
	}

	// We wanted to let OnceFlag a POD type.
	typedef volatile long OnceFlag;
	static const OnceFlag kOnceInit = 0;
	static const OnceFlag kOnceCalled = 1;

	template < typename Function >
	inline void CallOnlyOnce( OnceFlag & flag, Function function ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function();
		}
	}

	template < typename Function, typename T1 >
	inline void CallOnlyOnce( OnceFlag & flag, Function function, T1 a1 ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function( a1 );
		}
	}

	template < typename Function, typename T1, typename T2 >
	inline void CallOnlyOnce( OnceFlag & flag, Function function, T1 a1, T2 a2 ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function( a1, a2 );
		}
	}

	template < typename Function, typename T1, typename T2, typename T3 >
	inline void CallOnlyOnce( OnceFlag & flag, Function function, T1 a1, T2 a2, T3 a3 ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function( a1, a2, a3 );
		}
	}

	template < typename Function, typename T1, typename T2, typename T3, typename T4 >
	inline void CallOnlyOnce( OnceFlag & flag, Function function, T1 a1, T2 a2, T3 a3, T4 a4 ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function( a1, a2, a3, a4 );
		}
	}

	template < typename Function, typename T1, typename T2, typename T3, typename T4, typename T5 >
	inline void CallOnlyOnce( OnceFlag & flag, Function function, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5 ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function( a1, a2, a3, a4, a5 );
		}
	}

	template < typename Function, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
	inline void CallOnlyOnce( OnceFlag & flag, Function function, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6 ) 
	{
		if ( CompareAndExchange( flag, kOnceInit, kOnceCalled ) == kOnceInit )
		{
			function( a1, a2, a3, a4, a5, a6 );
		}
	}

	namespace type
	{
		class AtomicBool
		{
		public:
			AtomicBool( bool b )
			{
				AtomicallySet( flag_, conv( b ) );
			}

			AtomicBool( const AtomicBool & ab )
			{
				AtomicallySet( flag_, conv( ab ) );
			}

			AtomicBool & operator=( const AtomicBool & ab )
			{
				AtomicallySet( flag_, conv( ab ) );

				return *this;
			}

			AtomicBool & operator=( bool b )
			{
				AtomicallySet( flag_, conv( b ) );

				return *this;
			}

			operator bool() const
			{
				return conv( flag_ );
			}

		private:
			long conv( bool b ) const
			{
				return b ? 1 : 0;
			}

			bool conv( long n ) const
			{
				return ( 1 == n  ) ? true : false;
			}

		private:
			long flag_;
		};
	}

} }
