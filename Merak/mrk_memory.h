﻿#pragma once

#include <memory>

namespace Merak { namespace memory
{
	bool Initialize();
	
	void Finalize();

	void * AllocatePool( __in size_t size );
	
	void ReleasePool( __in void * buffer );

	// PoolAllocator template class
	// 
	// Merak 内部で STL コンテナを使用する場合に指定するアロケータ.
	// このアロケータを使用することで、ヒープ確保処理を制御する.
	// これにより、Merak で STL コンテナを使用することによるヒープ確保で HeapAllocate 
	// などのフックに捕まることを回避する.
	//
	// 注意点
	//   このアロケータは一回に確保するヒープサイズが決まっているので、
	//   要素を追加する度に連続した領域サイズが大きくなる vector はなるべく使用しない
	//   vector に要素を追加していき、全体の連続した領域サイズがあるタイミングでアロケ
	//   ータが一度に確保できるサイズを上回ると結果として std::bad_alloc 例外が発生する
	template < class T >
	class PoolAllocator
		: public std::allocator< T >
	{
	public:
		PoolAllocator() throw() {}
		PoolAllocator( const PoolAllocator & ) throw() {}
		template < class U > PoolAllocator( const PoolAllocator< U > & ) throw() {}

		pointer allocate( size_type num, const_pointer hint = 0 )
		{
			pointer ptr = ( pointer )( AllocatePool( num * sizeof( T ) ) );

			if ( 0 == ptr )
			{
				throw std::bad_alloc();
			}
		
			return ptr;
		}

		void deallocate( pointer p, size_type num )
		{
			ReleasePool( p );
		}

		template < class U >
		struct rebind
		{
			typedef PoolAllocator< U > other;
		};
	};

} } // namespace Merak::memory
