﻿#pragma once

#include <stdint.h>
#include <memory>

namespace Merak { namespace container
{
	template < typename T, class Allocator = std::allocator< T > >
	class Stack
	{
	public:
		typedef T								value_type;
		typedef Allocator						allocator_type;
		typedef typename Allocator::size_type	size_type;

	private:
		// リンク
		struct Link
		{
			Link * prev;
			Link * next;
		};
	
		// ヘッド
		struct Head
			: public Link
		{
			size_type size;
		};
	
		// ノード
		struct Node
			: public Link
		{
			value_type data;
		};

	private:
		typedef typename Allocator::template rebind< Node >::other		rebind_type;

	private:
		Node * create_node( const T & t )
		{
			Node * n = ralloc_.allocate( 1 );
		
			init_link( n );
		
			try
			{
				alloc_.construct( &( n->data ), t );
			}
			catch ( ... )
			{
				ralloc_.deallocate( n, 1 );
			}

			return n;
		}
	
		void delete_node( Node * n )
		{
			alloc_.destroy( &( n->data ) );

			ralloc_.deallocate( n, 1 );
		}
	
		void init_link( Link * i )
		{
			i->prev = i->next = i;
		}
	
		void init_head()
		{
			init_link( &head_ );
	
			head_.size = 0;
		}
	
		void delete_all_nodes()
		{
			for ( Link * i = head_.next; i != &head_; )
			{
				Node * r = reinterpret_cast< Node * >( i );
			
				i = i->next;
			
				delete_node( r );
			}
		}
	
	private:
		Head head_;				// ヘッド・ノード
		rebind_type ralloc_;	// ノードアロケータ
		Allocator alloc_;		// アロケータ
	
	public:
		Stack()
		{
			init_head();
		}

		~Stack()
		{
			delete_all_nodes();
		}
	
		Stack( const Stack< T, Allocator > & other )
		{
			init_head();
		
			for ( Link * i = other.head_.next; i != &other.head_; i = i->next )
			{
				Node * n = reinterpret_cast< Node * >( i );

				push( n->data );
			}
		}
	
		Stack & operator=( const Stack< T, Allocator > & other )
		{
			if ( this == &other )
			{
				return *this;
			}

			delete_all_nodes();
		
			for ( Link * i = other.head_.next; i != &other.head_; i = i->next )
			{
				Node * n = reinterpret_cast< Node * >( i );

				push( n->data );
			}

			return *this;
		}

		void pop()
		{
			if ( &head_ == head_.prev )
			{
				return;
			}
		
			Node * d = reinterpret_cast< Node * >( head_.prev );
		
			head_.prev = head_.prev->prev;
		
			head_.prev->next = &head_;
		
			head_.size --;
		
			delete_node( d );
		}

		void push( const T & t )
		{
			Node * n = create_node( t );

			n->next = &head_;

			n->prev = head_.prev;
		
			head_.prev->next = n;
		
			head_.prev = n;
		
			head_.size ++;
		}

		bool empty()
		{
			return &head_ == head_.prev;
		}

		size_type size()
		{
			return head_.size;
		}

		T & peek( size_t amount = 0 )
		{
			Link * link = head_.prev;

			for ( size_t i = 0; i < amount && i < size(); i ++ )
			{
				link = link->prev;
			}
			
			return reinterpret_cast< Node * >( link )->data;
		}
	};

	struct IntegerTraits
	{
		static uint32_t hash( __in const uint32_t & value )
		{
			return value;
		}

		static bool equals(
			__in const uint32_t & x, 
			__in const uint32_t & y )
		{
			return x == y;
		}
	};

	template < typename KeyTy, typename ValueTy, typename KeyTraits = IntegerTraits, size_t Capacity = 0x1000 >
	class FixedHashTable
	{
	public:
		FixedHashTable();
		FixedHashTable( const FixedHashTable & table ) {}
		~FixedHashTable() {}

		struct Element
		{
			KeyTy first;
			ValueTy second;
		private:
			bool active;
	
			friend class FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >;
		};
	
		class iterator
		{
		public:
			iterator operator++( int )
			{
				iterator old = *this;

				element_ = hashTable_->next( element_ );

				return old;
			}

			iterator & operator++()
			{
				element_ = hashTable_->next( element_ );

				return *this;
			}

			Element & operator*()
			{
				return *element_;
			}

			Element * operator->()
			{
				return element_;
			}

			bool operator==( __in const iterator & other )
			{
				return element_ == other.element_;
			}

			bool operator!=( __in const iterator & other )
			{
				return element_ != other.element_;
			}

		private:
			iterator(
				const FixedHashTable * hashTable,
				Element * element )
					: hashTable_( hashTable ), element_( element ) {}

		private:
			const FixedHashTable * hashTable_;
			Element * element_;

			friend class FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >;
		};

		iterator begin() const
		{
			return iterator( this, this->head() );
		}

		iterator end() const
		{
			return iterator( this, NULL );
		}

		iterator find( __in KeyTy key )
		{
			return iterator( this, this->lookup( key ) );
		}

		void erase( __in const iterator & it )
		{
			remove( it.element_ );
		}

		size_t size() const
		{
			return occupancy_;
		}

		size_t capacity() const
		{
			return capacity_;
		}

		Element * lookup( __in const KeyTy & key );

		iterator insert(
			__in const KeyTy & key,
			__in const ValueTy & value );

		void remove( __in const KeyTy & key );

		void clear();

	private:
		void remove( __in Element * element );

		Element * head() const;

		Element * tail() const
		{
			return const_cast< Element * >( table_ ) + capacity_; 
		}

		Element * next( __in Element * element ) const;

		void forward( __in Element * & element ) const
		{
			if ( ++ element >= tail() )
			{
				element = const_cast< Element * >( table_ );
			}
		}

		Element * get( __in const KeyTy & key );

	private:
		template < size_t V >
		struct Aligned
		{
			enum { width = 0x1000 };
			enum { size = ( V + ( width - 1 ) ) & ~( width - 1 ) };
		};

		size_t capacity_;
		size_t occupancy_;

		Element table_[Aligned< Capacity >::size];
	};

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::FixedHashTable()
		: capacity_( Aligned< Capacity >::size )
	{
		clear();
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	typename FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::Element *
	FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::lookup( __in const KeyTy & key )
	{
		Element * e = get( key );

		if ( e != NULL && e->active )
		{
			return e;
		}

		return NULL;
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	typename FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::iterator
	FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::insert(
		__in const KeyTy & key,
		__in const ValueTy & value )
	{
		Element * e = get( key );

		if ( e != NULL )
		{
			if ( e->active )
			{
				e->second = value;		// overwrite
			}
			else if ( occupancy_ < capacity_ )
			{
				e->first = key;
				e->second = value;
				e->active = true;

				occupancy_ ++;
			}
		}

		return iterator( this, e );
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	void FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::remove( __in Element * e )
	{
		Element * start = e;
		Element * n = e;

		do
		{
			// forward
			forward( n );

			if ( !n->active )
			{
				break;
			}

			Element * r = table_ + ( KeyTraits::hash( n->first ) % capacity_ );

			// relocation
			if ( ( n > e && ( r <= e || r > n ) ) || ( n < e && ( r <= e && r > n ) ) )
			{
				*e = *n;
				e = n;
			}
		}
		while ( n != start );

		e->active = false;

		occupancy_ --;
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	void FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::remove( __in const KeyTy & key )
	{
		Element * e = get( key );

		if ( e != NULL && e->active )
		{
			remove( e );
		}
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	void FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::clear()
	{
		const Element * ter = tail();

		for ( Element * e = table_; e < ter; e ++ )
		{
			e->active = false;
		}

		occupancy_ = 0;
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	typename FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::Element *
	FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::head() const
	{
		return next( const_cast< Element * >( table_ ) - 1 );
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	typename FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::Element *
	FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::next( __in Element * e ) const
	{
		const Element * ter = tail();

		for ( e ++; e < ter; e ++ )
		{
			if ( e->active )
			{
				return e;
			}
		}

		return NULL;
	}

	template < typename KeyTy, typename ValueTy, typename KeyTraits, size_t Capacity >
	typename FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::Element *
	FixedHashTable< KeyTy, ValueTy, KeyTraits, Capacity >::get( __in const KeyTy & key )
	{
		Element * e = table_ + ( KeyTraits::hash( key ) % capacity_ );
	
		const Element * start = e;
		const Element * ter = tail();

		while ( e->active 
			&& ( KeyTraits::hash( key ) != KeyTraits::hash( e->first )
				|| !KeyTraits::equals( key, e->first ) ) )
		{
			// forward
			forward( e );

			if ( e == start )
			{
				return NULL;		// detect turned around
			}
		}

		return e;
	}

} } // namespace Merak::container