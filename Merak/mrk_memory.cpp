﻿#include "mrk_memory.h"
#include "mrk_helper.h"
#include "mrk_wrapper.h"
#include "mrk_singleton.h"
#include "mrk_defines.h"

namespace Merak { namespace memory { namespace
{
	class MemoryPool
		: private Merak::helper::Noncopyable
	{
	private:
		struct BlockList
		{
			BlockList * next;
		};

		struct Header
		{
			Header * next;
			uint32_t flags;
			size_t size;
		};

	public:
		void * allocate( __in size_t size );
		void release( __in void * buffer );
		
		static bool CreateInstance()
		{
			try
			{
#if ( MERAK_INITIAL_ALLOC_SIZE > 0 )
				Merak::singleton::Singleton< MemoryPool >::Initialize( MERAK_DEFAULT_INITIAL_ALLOC_SIZE, MERAK_INITIAL_ALLOC_SIZE );
#else
				Merak::singleton::Singleton< MemoryPool >::Initialize( MERAK_DEFAULT_INITIAL_ALLOC_SIZE );
#endif
				return true;
			}
			catch ( ... )
			{
			}

			return false;
		}

		static void DeleteInstance()
		{
			Merak::singleton::Singleton< MemoryPool >::Finalize();
		}

		static MemoryPool * GetInstance()
		{
			return Merak::singleton::Singleton< MemoryPool >::Get();
		}

	private:
		explicit MemoryPool::MemoryPool(
			__in size_t allocSize, 
			__in size_t initialAllocSize = 0,
			__in uint32_t maxAllocCount = 0 );
		~MemoryPool();

		inline Header * nextHeader( __in const Header * header ) const
		{
			return reinterpret_cast< Header * >( ( size_t )( header + 1 ) + header->size );
		}

		bool resetBlock();

		Header * divideBlock( __in size_t size );

		Header * findBlock( Header * target );

		void connectBlock( Header * target );

		Header * openBlock();

		Header * addBlock();

		void closeBlock();

	private:
		size_t allocCount_;
		BlockList * blockList_;
		Header * freeList_;
		
		const size_t kHeaderSize;
		const size_t kAllocSize;
		const size_t kInitialAllocSize;
		const uint32_t kAllocKey;
		const uint32_t kMaxAllocCount;

		friend class Merak::singleton::Singleton< MemoryPool >;
	};

} } } // namespace Merak::memory::{unnamed}

namespace Merak { namespace memory { namespace
{
	MemoryPool::MemoryPool(
		__in size_t allocSize, 
		__in size_t initialAllocSize,
		__in uint32_t maxAllocCount )
		: allocCount_( 0 )
		, blockList_( NULL )
		, freeList_( NULL )
		, kHeaderSize( sizeof( Header ) )
		, kAllocSize( allocSize )
		, kInitialAllocSize( initialAllocSize < allocSize ? allocSize : initialAllocSize )
		, kAllocKey( 0xCAFEBABE )
		, kMaxAllocCount( maxAllocCount ) {}

	MemoryPool::~MemoryPool()
	{
		closeBlock();
	}

	void * MemoryPool::allocate( __in size_t size )
	{
		using namespace Merak::helper;
		
		size_t requiredSize = Align< sizeof( void * ) >( size );

		if ( 0 >= requiredSize || Align< sizeof( void * ) >( kAllocSize ) < requiredSize )
		{
			return NULL;
		}

		if ( !resetBlock() )
		{
			return NULL;
		}

		Header * available = divideBlock( requiredSize );

		if ( NULL == available )
		{
			return NULL;
		}

		return reinterpret_cast< void * >( available + 1 );
	}

	void MemoryPool::release( __in void * buffer )
	{
		if ( 0 == allocCount_ )
		{
			return;
		}

		if ( NULL != buffer )
		{
			Header * free = reinterpret_cast< Header * >( buffer ) - 1;

			if ( kAllocKey != free->flags )
			{
				// アロケートヘッダーが壊れている。
				return;
			}

			if ( NULL == freeList_ )
			{
				freeList_ = free;
				freeList_->next = freeList_;
			}
			else
			{
				// 解放ブロック位置検索
				freeList_ = findBlock( free );

				// 連結処理
				connectBlock( free );
			}

			// キーをリセット
			free->flags = 0;
		}
	}

	bool MemoryPool::resetBlock()
	{
		if ( 0 == allocCount_ )
		{
			// 初動時のみ実行
			// kInitialAllocSize 分の領域を kAllocSize 毎に分割して確保する
			uint32_t count = ( kInitialAllocSize + ( kAllocSize - 1 ) ) / kAllocSize;
			
			for ( uint32_t i = 0; i < count; i ++ )
			{
				if ( NULL == addBlock() )
				{
					return false;
				}
			}
		}
		else if ( NULL == freeList_ )
		{
			// MemoryPool::divideBlock 処理によって、
			// freeList_ が NULL になることは起こり得る

			if ( NULL == ( freeList_ = openBlock() ) )
			{
				return false;
			}
		}

		return true;
	}

	MemoryPool::Header * MemoryPool::divideBlock( __in size_t size )
	{
		for ( Header * p = freeList_, * n = p->next; ; p = n, n = n->next )
		{
			// 使用可能なブロックを発見
			if ( n->size >= size )
			{
				// 残りブロックサイズがヘッダーサイズ未満
				// n->size == size のケースも含まれる
				if ( n->size - size < kHeaderSize )
				{
					if ( p->next != n->next )
					{
						// --- p --- n ---
						// --- p --- n --- nn ---
						// --- p --- n -- nn --- nnn ---
						// というように少なくとも２つ以上ブロックがある状態の場合で n を取得. 
						// このとき freeList_ が n を指している可能性があるので、
						// freeList_ を p に指しなおす
						freeList_ = p;

						// n をリストから削除
						p->next = n->next;
					}
					else
					{
						// 唯一のブロックを取得
						freeList_ = NULL;
					}
				}
				else
				{
					// ブロックサイズが要求サイズより大きいので分割
					// 分割した後方部分をユーザーに渡す
					n->size -= kHeaderSize + size;
					n = nextHeader( n );
					n->size = size;
				}

				// アロケートキーを設定
				n->flags = kAllocKey;

				return n;
			}

			if ( n == freeList_ )
			{
				// 一周した
				if ( NULL == ( n = addBlock() ) )
				{
					break;
				}
			}
		}

		return NULL;
	}

	MemoryPool::Header * MemoryPool::findBlock( Header * target )
	{
		Header * found = freeList_;
		
		do
		{
			// | ----- fouud ------- target -------- found->next ---- |
			if ( found < found->next && ( found < target && target < found->next ) )
			{
				break;
			}

			// | -------- found->next | found ---------------- target |
			// | ----- target ----- found->next --------------- found |
			if ( found >= found->next && ( found < target || target < found->next ) )
			{
				break;
			}
				
			found = found->next;
		}
		while ( found != freeList_ );

		return found;
	}

	void MemoryPool::connectBlock( Header * target )
	{
		if ( nextHeader( target ) == freeList_->next )
		{
			// | ------- found ------ target + found->next ----- |
			// | ------- target + found->next ------------ found |
			target->size += freeList_->next->size + kHeaderSize;
			
			if ( freeList_->next == freeList_ )
			{
				// freeList_ が単独で存在する場合、
				// target を freeList_ として扱う
				freeList_ = target;
			}
			else
			{
				target->next = freeList_->next->next;
			}
		}
		else
		{
			// | --- found ---- target ----- found->next ------- |
			// | ------- target ----- found->next -------- found |
			target->next = freeList_->next;
		}

		if ( nextHeader( freeList_ ) == target )
		{
			// | -------- found + target ------ found->next ---- |
			// | ------- found->next ------------ found + target |
			freeList_->size += target->size + kHeaderSize;
			freeList_->next = target->next;
		}
		else
		{
			// | ------- found ---- target ----- found->next ----|
			// | ---- found->next --------- found ------- target |
			freeList_->next = target;
		}
	}

	MemoryPool::Header * MemoryPool::openBlock()
	{
		using namespace Merak::helper;
		using namespace Merak::wrapper;
		
		if ( 0 < kMaxAllocCount && kMaxAllocCount <= allocCount_ )
		{
			return NULL;
		}

		allocCount_ ++;

		size_t requiredSize = Align< sizeof( void * ) >( kAllocSize );
		BlockList * * tail = &blockList_;

		for ( ; *tail; tail = &(*tail)->next );

		*tail = SafeInvoker< BlockList * >::Execr(
			::VirtualAlloc,
			static_cast< LPVOID >( NULL ),
			requiredSize + kHeaderSize + sizeof( BlockList ), 
			MEM_COMMIT | MEM_RESERVE | MEM_TOP_DOWN,
			PAGE_EXECUTE_READWRITE );

		if ( NULL == *tail )
		{
			return NULL;
		}

		(*tail)->next = NULL;

		Header * header = reinterpret_cast< Header * >( *tail + sizeof( BlockList ) );

		header->next = header;
		header->flags = 0;
		header->size = requiredSize;

		return header;
	}

	MemoryPool::Header * MemoryPool::addBlock()
	{
		Header * header = openBlock();

		if ( NULL == header )
		{
			return NULL;
		}

		header->flags = kAllocKey;

		this->release( header + 1 );

		return freeList_;
	}

	void MemoryPool::closeBlock()
	{
		for ( BlockList * it = blockList_; it; )
		{
			BlockList * next = it->next;
			
			::VirtualFree( it, 0, MEM_RELEASE );

			it = next;
		}

		allocCount_ = 0;
		blockList_ = NULL;
		freeList_ = NULL;
	}

} } } // namespace Merak::memory::{unnamed}

namespace Merak { namespace memory
{
	bool Initialize()
	{
		return MemoryPool::CreateInstance();
	}

	void Finalize()
	{
		MemoryPool::DeleteInstance();
	}

	void * AllocatePool( __in size_t size )
	{
		if ( NULL == MemoryPool::GetInstance() )
		{
			return NULL;
		}

		return MemoryPool::GetInstance()->allocate( size );
	}

	void ReleasePool( __in void * buffer )
	{
		if ( NULL == MemoryPool::GetInstance() )
		{
			return;
		}

		MemoryPool::GetInstance()->release( buffer );
	}

} } // namespace Merak::memory
