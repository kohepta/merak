﻿#pragma once

#include "mrk_hook.h"
#include "mrk_helper.h"
#include "hde32/hde32.h"

namespace Merak { namespace hook { namespace impl
{
	typedef hde32s HDEObject;

	inline uintptr_t * GetRelayTable( __in const HookObject * object )
	{
		return reinterpret_cast< uintptr_t * >( reinterpret_cast< uintptr_t >( object->bridge.bytes ) );
	}

	inline uint8_t * GetBridgeCode( __in const HookObject * object )
	{
		return reinterpret_cast< uint8_t * >(
			reinterpret_cast< uintptr_t >( GetRelayTable( object ) )
				+ object->bridge.relayTableLength );
	}

	inline uintptr_t * GetAlternativeRelayTable( __in const PrologObject * object )
	{
		return reinterpret_cast< uintptr_t * >( reinterpret_cast< uintptr_t >( object->bytes ) );
	}

	inline uint8_t * GetPreAlternativeProlog( __in const PrologObject * object )
	{
		return reinterpret_cast< uint8_t * >(
			reinterpret_cast< uintptr_t >( GetAlternativeRelayTable( object ) )
				+ object->length.relayTable );
	}

	inline uint8_t * GetAlternativeProlog( __in const PrologObject * object )
	{
		return reinterpret_cast< uint8_t * >(
			reinterpret_cast< uintptr_t >( GetPreAlternativeProlog( object ) )
				+ object->length.preAlternative );
	}

	inline uint8_t * GetOriginalProlog( __in const PrologObject * object )
	{
		return reinterpret_cast< uint8_t * >(
			reinterpret_cast< uintptr_t >( GetAlternativeProlog( object ) )
				+ object->length.alternative );
	}

	inline void BackupOriginalProlog(
		__in const void * originalProlog,
		__in const HookObject * object )
	{
		uint8_t * dst = GetOriginalProlog( object->prolog );
		const uint8_t * src = reinterpret_cast< const uint8_t * >( originalProlog );

		Merak::helper::Copy( dst, src, object->prolog->length.replacement );
	}

	inline unsigned int HDEDisassemble(
		__in const void * code, 
		__out HDEObject & hdeObj )
	{
		return hde32_disasm( code, &hdeObj );
	}

	PrologObject * CreatePrologObject( __in const PrologLength & prologLength );

	HookObject * CreateHookObject(
		__in const void * targetFunction,
		__in const PrologLength & prologLength,
		__in uint8_t relayTableIndex,
		__in uint8_t codeLength );

	bool SelectAttachStyle( __in HookObject * object );

	class AlternativeProlog
		: Merak::helper::Noncopyable
	{
	public:
		explicit AlternativeProlog( __in const void * targetFunction );

		PrologLength getLength( __in size_t replacement );

		bool build( __out PrologObject * object );

	private:
		class Builder
		{
		public:
			explicit Builder( __in PrologObject * object );

			uintptr_t makeRelayTable( __in uintptr_t dest );

			void makePreAlternative();

			void copyCode(
				__in uintptr_t position,
				__in const void * code,
				__in size_t codeSize );

			void fixOriginalPrologLength( __in size_t size );
			void fixAlternativePrologLength( __in size_t size );
			void fixRalayTableLength();
			void fixReplacementLegnth( __in size_t size );
	
		private:
			PrologObject * prologObject_;
			uint8_t relayTableIndex_;
			const bool fixed_;
		};

	private:
		bool disassemble(
			__in size_t replacement,
			__out PrologObject * object );

		bool insideJump(
			__in const void * targetFunction, 
			__in uintptr_t destination,
			__in size_t replacement );

		bool isJumping(
			__in const void * pc,
			__in uintptr_t destination );

		void setJccOpcode(
			__in const HDEObject & hdeObj,
			__out RelJcc & jcc );

		void setJccOpcode(
			__in const HDEObject & hdeObj,
			__out AbsJcc & jcc );

		uintptr_t getAbsoluteDestinationAddress(
			__in const HDEObject & hdeObj,
			__in const uint8_t * inst );

	private:
		const void * target_;
	};

} } } // namespace Merak::hook::impl