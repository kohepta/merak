﻿#pragma once

#include <stdint.h>

namespace Merak { namespace tls
{
	bool Initialize();

	void Finalize();

	bool __stdcall IsDisabledSlot();

	void __stdcall DisableSlot();

	void __stdcall EnableSlot();

	void __stdcall DisableAllSlots();

	void __stdcall EnableAllSlots();

	uint32_t __stdcall GetDepthOfCall();

	bool __stdcall IncreaseDepthOfCall();

	void __stdcall DecreaseDepthOfCall();

	void __stdcall ClearDepthOfCall();

	void __stdcall SaveLastError();

	void __stdcall LoadLastError();

	void __stdcall SaveArguments( uint32_t esp );

	const uint32_t * __stdcall GetArguments();

	void __stdcall ClearArguments();

} } // namespace Merak::tls