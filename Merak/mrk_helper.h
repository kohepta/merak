﻿#pragma once

#include <Windows.h>
#include <stdint.h>

namespace Merak { namespace helper
{
	// Noncopyable template class
	// インスタンスコピー防止クラス
	// コピーさせたくないクラスにこのクラスを private 継承させる
	class Noncopyable
	{
	protected:
		Noncopyable() {}
		~Noncopyable() {}

	private:
		Noncopyable( __in const Noncopyable & other );
		Noncopyable & operator = ( __in const Noncopyable & other );
	};

	class ScopedHandle
		: private Noncopyable
	{
	public:
		ScopedHandle( HANDLE handle ) : handle_( handle ) {}
		~ScopedHandle()
		{
			CloseHandle( handle_ );
		}

		operator HANDLE() const
		{
			return handle_;
		}

	private:
		HANDLE handle_;
	};

	// メモリにデータを書き込む
	// 書き込んだ際、指定インデックスは書き込んだサイズ分インクリメントされる
	template < typename T, typename U >
	inline void WriteMemory(
		__in void * buffer,
		__in U data,
		__out uint32_t & index )
	{
		T * dst = reinterpret_cast< T * >( buffer );
		T src = force_cast< T, U >( data );

		*dst = src;

		index += sizeof( T );
	}

	template < typename T, typename U >
	inline void WriteMemory(
		__in void * buffer,
		__in U data )
	{
		uint32_t dummy = 0;

		WriteMemory< T, U >( buffer, data, dummy );
	}

	template < typename T, typename U >
	inline uintptr_t AddAddress(
		__in T addr1,
		__in U addr2 )
	{
		uintptr_t ptraddr1 = force_cast< uintptr_t, T >( addr1 );
		uintptr_t ptraddr2 = force_cast< uintptr_t, U >( addr2 );
		
		return ptraddr1 + ptraddr2;
	}

	template < typename T, typename U >
	inline uintptr_t SubAddress(
		__in T addr1,
		__in U addr2 )
	{
		uintptr_t ptraddr1 = force_cast< uintptr_t, T >( addr1 );
		uintptr_t ptraddr2 = force_cast< uintptr_t, U >( addr2 );
		
		return ptraddr1 - ptraddr2;
	}

	// アラインメント
	template < size_t Size >
	inline size_t Align( __in size_t d )
	{
		return ( d + ( Size - 1 ) ) & ~( Size - 1 );
	}

	// memcpy の代替
	inline void Copy(
		__out void * dst,
		__in const void * src,
		__in size_t size,
		__in size_t max = 0 )
	{
		uint8_t * _dst = reinterpret_cast< uint8_t * >( dst );
		const uint8_t * _src = reinterpret_cast< const uint8_t * >( src );

		if ( max > 0 && size > max )
		{
			size = max;
		}

		for ( size_t i = 0; i < size; _dst[i] = _src[i], i ++ );
	}

	inline int Compare(
		__in const void * op1,
		__in const void * op2,
		__in size_t size )
	{
		const uint8_t * _op1 = reinterpret_cast< const uint8_t * >( op1 );
		const uint8_t * _op2 = reinterpret_cast< const uint8_t * >( op2 );
		
		if ( 0 >= size )
		{
			return 0;
		}

		for ( size_t i = 0; *_op1 == *_op2 && i < size - 1; i ++, _op1 ++, _op2 ++ );

		return *_op1 - *_op2;
	}

	inline void Fill(
		__in void * dst,
		__in uint8_t value,
		__in size_t size )
	{
		uint8_t * _dst = reinterpret_cast< uint8_t * >( dst );

		for ( size_t i = 0; i < size; _dst[i] = value, i ++ );
	}

	template < typename T, typename S >
	inline T force_cast( const S & s )
	{
		union { T t; S s; } u = { 0 };

		u.s = s;

		return u.t;
	}

} } // namespace Merak::helper
