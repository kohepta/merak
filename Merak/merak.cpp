﻿#include <unordered_map>
#include <algorithm>
#include "merak.h"
#include "mrk_lock.h"
#include "mrk_hook.h"
#include "mrk_stub.h"
#include "mrk_memory.h"
#include "mrk_tls.h"
#include "mrk_thread.h"
#include "mrk_singleton.h"
#include "mrk_defines.h"

#define __STRING( x )		#x
#define STRING( x )			__STRING( x )

#pragma message ( "" )
#pragma message ( "  + ---------------- The specified options of the \"Merak\". ----------------" )
#pragma message ( "  +" )
#if ( MERAK_INITIAL_ALLOC_SIZE > 0 )
	#pragma message ( "  +  [MERAK_INITIAL_ALLOC_SIZE               ]: " STRING( MERAK_INITIAL_ALLOC_SIZE ) )
#else
	#pragma message ( "  +  [MERAK_INITIAL_ALLOC_SIZE               ]: Default Size - " STRING( MERAK_DEFAULT_INITIAL_ALLOC_SIZE ) )
#endif

#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
	#pragma message ( "  +  [MERAK_ENABLE_LAST_ERROR_PROTECTION     ]: True" )
#else 
	#pragma message ( "  +  [MERAK_ENABLE_LAST_ERROR_PROTECTION     ]: False" )
#endif

#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
	#pragma message ( "  +  [MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS]: " STRING( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS ) )
#else
	#pragma message ( "  +  [MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS]: 0" )
#endif

#if ( MERAK_SPINLOCK_TIMEOUT > 0 )
	#pragma message ( "  +  [MERAK_SPINLOCK_TIMEOUT                 ]: " STRING( MERAK_SPINLOCK_TIMEOUT ) )
#else
	#pragma message ( "  +  [MERAK_SPINLOCK_TIMEOUT                 ]: -" )
#endif
#pragma message ( "  +" )
#pragma message ( "  + ----------------------------------------------------------------------- " )
#pragma message ( "" )

namespace Merak { namespace
{
	class HookInterface
		: private Merak::helper::Noncopyable
	{
	public:
		Handle HookInterface::installNativeWrapperHook(
			__in CC::type cc,
			__in const void * targetFunction,
			__in const void * wrapperFunction, 
			__in bool force = false );

		Handle installWrapperHook(
			__in CC::type cc,
			__in const void * targetFunction,
			__in const void * wrapperFunction,
			__in bool force = false );

		Handle installPrePostHook(
			__in const void * targetFunction,
			__in const char * targetFunctionName,
			__in const PreHookFunction preFunction,
			__in const PostHookFunction postFunction,
			__in bool force = false,
			__in WORD retArgSize = 0 );

		bool attachHook( __in Handle handle );

		void detachHook( __in Handle handle );

		void restoreAllHooks();

		static bool CreateInstance()
		{
			try
			{
				Merak::singleton::Singleton< HookInterface >::Initialize();

				return true;
			}
			catch ( ... )
			{
			}

			return false;
		}

		static void DeleteInstance()
		{
			Merak::singleton::Singleton< HookInterface >::Finalize();
		}

		static HookInterface * GetInstance()
		{
			return Merak::singleton::Singleton< HookInterface >::Get();
		}

	private:
		HookInterface() : spinWait_(), attachedHandles_() {}
		~HookInterface()
		{
			restoreAllHooks();
		}

		bool isExecutableArea( __in const void * address );

		bool detachHookImpl( __in Merak::hook::HookObject * object );

	private:
		typedef std::unordered_map< uintptr_t, Handle >	HandleMap;

		Merak::lock::SpinWait spinWait_;
		HandleMap attachedHandles_;

		friend class Merak::singleton::Singleton< HookInterface >;
	};

} } // namespace ::{unnamed}

namespace Merak { namespace
{
	Handle HookInterface::installNativeWrapperHook(
		__in CC::type cc,
		__in const void * targetFunction,
		__in const void * wrapperFunction,
		__in bool force /* = false */ )
	{
		using namespace Merak::hook;
		using namespace Merak::lock;

		HookObject * object = NULL;

		ScopedLock lock( spinWait_ );

		if ( !isExecutableArea( targetFunction ) || !isExecutableArea( wrapperFunction ) )
		{
			return NULL;
		}

		PrologLength prologLength = GetPrologLength( targetFunction, sizeof( RelJmp ) );

		if ( prologLength > 0 )
		{
			HookType::type ht;

			switch ( cc )
			{
			case CC::__STDCALL:
			case CC::__CDECL:
				ht = HookType::NATIVE_WRAPPER;
				break;
			default:
				return NULL;
			}

			object = CreateNativeWrapperHook( cc, targetFunction, wrapperFunction, prologLength, force );
			
			if ( NULL != object )
			{
				object->hookType = ht;
			}
		}

		return object;
	}

	Handle HookInterface::installWrapperHook(
		__in CC::type cc,
		__in const void * targetFunction,
		__in const void * wrapperFunction,
		__in bool force /* = false */ )
	{
		using namespace Merak::hook;
		using namespace Merak::lock;

		HookObject * object = NULL;

		ScopedLock lock( spinWait_ );

		if ( !isExecutableArea( targetFunction ) || !isExecutableArea( wrapperFunction ) )
		{
			return NULL;
		}

		PrologLength prologLength = GetPrologLength( targetFunction, sizeof( RelJmp ) );

		if ( prologLength > 0 )
		{
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
			prologLength.preAlternative = Merak::stub::PreAlternative::GetCodeSize();
#endif
			HookType::type ht;

			switch ( cc )
			{
			case CC::__STDCALL:
			case CC::__CDECL:
				ht = HookType::WRAPPER;
				break;
			default:
				return NULL;
			}

			object = CreateWrapperHook( cc, targetFunction, wrapperFunction, prologLength, force );
			
			if ( NULL != object )
			{
				object->hookType = ht;
			}
		}

		return object;
	}

	Handle HookInterface::installPrePostHook(
		__in const void * targetFunction,
		__in const char * targetFunctionName,
		__in const PreHookFunction preFunction,
		__in const PostHookFunction postFunction,
		__in bool force, /* = false */
		__in WORD retArgSize /* = 0 */ )
	{
		using namespace Merak::hook;
		using namespace Merak::lock;
		using namespace Merak::helper;

		HookObject * object = NULL;

		ScopedLock lock( spinWait_ );

		if ( !isExecutableArea( targetFunction ) || !isExecutableArea( preFunction ) )
		{
			return NULL;
		}

		PrologLength prologLength = GetPrologLength( targetFunction, sizeof( RelJmp ) );

		if ( prologLength > 0 )
		{
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
			prologLength.preAlternative = Merak::stub::PreAlternative::GetCodeSize();
#endif
			if ( object = CreatePrePostHook( 
				targetFunction, 
				preFunction, 
				postFunction, 
				prologLength,
				force,
				retArgSize ) )
			{
				if ( NULL != postFunction )
				{
					object->hookType = hook::HookType::PREPOST;
				}
				else
				{
					object->hookType = hook::HookType::PRE;
				}

				Copy(
					object->targetFunctionName, 
					targetFunctionName, 
					std::strlen( targetFunctionName ),
					sizeof( object->targetFunctionName ) - 1 );
			}
		}

		return object;
	}

	bool HookInterface::attachHook( __in Handle handle )
	{
		using namespace Merak::hook;
		using namespace Merak::lock;

		bool success = false;

		HookObject * object = reinterpret_cast< HookObject * >( handle );

		ScopedLock lock( spinWait_ );

		if ( handle && isExecutableArea( object->targetFunction ) )
		{
			// 対象関数プロローグ書き換え
			if ( success = ReplaceProlog( object ) )
			{
				object->attached = true;

				try
				{
					HandleMap::iterator it =
						attachedHandles_.find( reinterpret_cast< uintptr_t >( object->targetFunction ) );
					
					if ( it == attachedHandles_.end() )
					{
						attachedHandles_.insert( std::make_pair< uintptr_t >(
							reinterpret_cast< uintptr_t >( object->targetFunction ),
							handle ) );
					}
				}
				catch ( ... )
				{
				}
			}
		}

		return success;
	}

	void HookInterface::detachHook( __in Handle handle )
	{
		using namespace Merak::hook;
		using namespace Merak::lock;

		HookObject * object = reinterpret_cast< HookObject * >( handle );

		ScopedLock lock( spinWait_ );

		detachHookImpl( object );
	}

	void HookInterface::restoreAllHooks()
	{
		using namespace Merak::hook;
		using namespace Merak::lock;

		ScopedLock lock( spinWait_ );

		for ( HandleMap::iterator it = attachedHandles_.begin();
			it != attachedHandles_.end();
			it ++ )
		{
			HookObject * object = reinterpret_cast< HookObject * >( it->second );

			detachHookImpl( object );
		}
	}
	
	bool HookInterface::isExecutableArea( __in const void * address )
	{
		static const DWORD protectMask
			= ( PAGE_EXECUTE | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY );

		if ( NULL == address )
		{
			return false;
		}

		MEMORY_BASIC_INFORMATION mi = { 0 };
		::VirtualQuery( address, &mi, sizeof( mi ) );

		return ( 0 != ( mi.Protect & protectMask ) );
	}

	bool HookInterface::detachHookImpl( __in Merak::hook::HookObject * object )
	{
		using namespace hook;

		bool success = false;

		if ( object && object->attached && isExecutableArea( object->targetFunction ) )
		{
			// 対象関数プロローグをこのフックがアタッチされる直前の状態に戻す
			if ( success = RestoreProlog( object ) )
			{
				object->attached = false;
			}
		}

		return success;
	}

} } // namespace Merak::{unnamed}

bool __stdcall MerakInitialize()
#pragma comment( linker, "/export:MerakInitialize=_MerakInitialize@0" )
{
	if ( !Merak::tls::Initialize() )
	{
		return false;
	}

	if ( !Merak::memory::Initialize() )
	{
		return false;
	}

	if ( !Merak::thread::Initialize() )
	{
		return false;
	}

	if ( !Merak::HookInterface::CreateInstance() )
	{
		return false;
	}

	return true;
}

void __stdcall MerakFinalize()
#pragma comment( linker, "/export:MerakFinalize=_MerakFinalize@0" )
{
	Merak::tls::DisableSlot();
	
	Merak::HookInterface::DeleteInstance();

	Merak::thread::Finalize();

	Merak::memory::Finalize();

	Merak::tls::Finalize();
}

Merak::Handle __stdcall MerakInstallNativeWrapperHook(
	__in Merak::CC::type cc,
	__in const void * targetFunction,
	__in const void * wrapperFunction,
	__in bool force /* = false */ )
#pragma comment( linker, "/export:MerakInstallNativeWrapperHook=_MerakInstallNativeWrapperHook@16" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return NULL;
	}

	Merak::Handle handle = NULL;

	Merak::tls::DisableSlot();
	{
		handle = Merak::HookInterface::GetInstance()->installNativeWrapperHook(
			cc,
			targetFunction,
			wrapperFunction,
			force );
	}
	Merak::tls::EnableSlot();

	return handle;
}

Merak::Handle __stdcall MerakInstallWrapperHook(
	__in Merak::CC::type cc,
	__in const void * targetFunction,
	__in const void * wrapperFunction,
	__in bool force /* = false */ )
#pragma comment( linker, "/export:MerakInstallWrapperHook=_MerakInstallWrapperHook@16" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return NULL;
	}

	Merak::Handle handle = NULL;

	Merak::tls::DisableSlot();
	{
		handle = Merak::HookInterface::GetInstance()->installWrapperHook(
			cc,
			targetFunction,
			wrapperFunction,
			force );
	}
	Merak::tls::EnableSlot();

	return handle;
}

Merak::Handle __stdcall MerakInstallPrePostHook(
	__in const void * targetFunction,
	__in const char * targetFunctionName,
	__in const Merak::PreHookFunction preFunction,
	__in const Merak::PostHookFunction postFunction,
	__in bool force /* = false */,
	__in WORD retArgSize /* = 0 */ )
#pragma comment( linker, "/export:MerakInstallPrePostHook=_MerakInstallPrePostHook@24" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return NULL;
	}

	Merak::Handle handle = NULL;

	Merak::tls::DisableSlot();
	{
		handle = Merak::HookInterface::GetInstance()->installPrePostHook(
			targetFunction,
			targetFunctionName,
			preFunction,
			postFunction,
			force,
			retArgSize );
	}
	Merak::tls::EnableSlot();

	return handle;
}

void __stdcall MerakUninstallHook( __in Merak::Handle handle )
#pragma comment( linker, "/export:MerakUninstallHook=_MerakUninstallHook@4" )
{
	if ( NULL == Merak::HookInterface::GetInstance() || NULL == handle )
	{
		return;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	Merak::hook::DeleteHookObject( object );
}

bool __stdcall MerakAttachHook( __in Merak::Handle handle )
#pragma comment( linker, "/export:MerakAttachHook=_MerakAttachHook@4" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return NULL;
	}

	bool success = false;

	Merak::tls::DisableSlot();
	{
		success = Merak::HookInterface::GetInstance()->attachHook( handle );
	}
	Merak::tls::EnableSlot();

	return success;
}

void __stdcall MerakDetachHook( __in Merak::Handle handle )
#pragma comment( linker, "/export:MerakDetachHook=_MerakDetachHook@4" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::DisableSlot();
	{
		Merak::HookInterface::GetInstance()->detachHook( handle );
	}
	Merak::tls::EnableSlot();
}

void __stdcall MerakEnterHookSafeArea()
#pragma comment( linker, "/export:MerakEnterHookSafeArea=_MerakEnterHookSafeArea@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::DisableSlot();
}

void __stdcall MerakLeaveHookSafeArea()
#pragma comment( linker, "/export:MerakLeaveHookSafeArea=_MerakLeaveHookSafeArea@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::EnableSlot();
}

void __stdcall MerakSuspend()
#pragma comment( linker, "/export:MerakSuspend=_MerakSuspend@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::DisableAllSlots();
}
	
void __stdcall MerakResume()
#pragma comment( linker, "/export:MerakResume=_MerakResume@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::EnableAllSlots();
}

uint32_t __stdcall MerakGetDepthOfCall()
#pragma comment( linker, "/export:MerakGetDepthOfCall=_MerakGetDepthOfCall@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return 0;
	}

	return Merak::tls::GetDepthOfCall();
}

bool __stdcall MerakIncreaseDepthOfCall()
#pragma comment( linker, "/export:MerakIncreaseDepthOfCall=_MerakIncreaseDepthOfCall@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return 0;
	}

	return Merak::tls::IncreaseDepthOfCall();
}

void __stdcall MerakDecreaseDepthOfCall()
#pragma comment( linker, "/export:MerakDecreaseDepthOfCall=_MerakDecreaseDepthOfCall@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::DecreaseDepthOfCall();
}

void __stdcall MerakClearDepthOfCall()
#pragma comment( linker, "/export:MerakClearDepthOfCall=_MerakClearDepthOfCall@0" )
{
	if ( NULL == Merak::HookInterface::GetInstance() )
	{
		return;
	}

	Merak::tls::ClearDepthOfCall();
}

void * __stdcall MerakGetAlternativeFunction( __in const Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetAlternativeFunction=_MerakGetAlternativeFunction@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	if ( object->hookType == Merak::hook::HookType::PRE 
		|| object->hookType == Merak::hook::HookType::PREPOST )
	{
		return NULL;
	}

	return Merak::hook::GetAlternativeFunction( object );
}

const void * __stdcall MerakGetOriginalFunction( __in const Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetOriginalFunction=_MerakGetOriginalFunction@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	return reinterpret_cast< Merak::hook::HookObject * >( handle )->targetFunction;
}

const char * __stdcall MerakGetOriginalFunctionName( __in const Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetOriginalFunctionName=_MerakGetOriginalFunctionName@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	if ( object->hookType == Merak::hook::HookType::WRAPPER )
	{
		return NULL;
	}

	return reinterpret_cast< Merak::hook::HookObject * >( handle )->targetFunctionName;
}

const uint32_t * __stdcall MerakGetOriginalEspValue( __in const Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetOriginalEspValue=_MerakGetOriginalEspValue@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	if ( object->hookType == Merak::hook::HookType::NATIVE_WRAPPER )
	{
		return NULL;
	}

	uint32_t espvalue = 0;

	if ( object->hookType == Merak::hook::HookType::WRAPPER
		|| object->hookType == Merak::hook::HookType::PRE )
	{
		Merak::thread::PeekContext( &espvalue, NULL );
	}

	if ( object->hookType == Merak::hook::HookType::PREPOST )
	{
		Merak::thread::PeekContext( &espvalue, NULL, 1 );
	}

	return reinterpret_cast< uint32_t * >( espvalue );
}

uintptr_t __stdcall MerakGetOriginalEspReference( __in const Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetOriginalEspReference=_MerakGetOriginalEspReference@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	if ( object->hookType == Merak::hook::HookType::NATIVE_WRAPPER )
	{
		return NULL;
	}

	uintptr_t espvreference = NULL;

	if ( object->hookType == Merak::hook::HookType::WRAPPER
		|| object->hookType == Merak::hook::HookType::PRE )
	{
		Merak::thread::PeekContext( NULL, &espvreference );
	}

	if ( object->hookType == Merak::hook::HookType::PREPOST )
	{
		Merak::thread::PeekContext( NULL, &espvreference, 1 );
	}

	return espvreference;
}

const uint32_t * __stdcall MerakGetPreHookSavedArguments( __in const Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetPreHookSavedArguments=_MerakGetPreHookSavedArguments@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	if ( object->hookType == Merak::hook::HookType::PREPOST )
	{
		const uint32_t * esp = Merak::tls::GetArguments();

		if ( NULL == esp )
		{
			uint32_t espvalue;

			Merak::thread::PeekContext( &espvalue, NULL, 1 );

			return reinterpret_cast< uint32_t * >( espvalue );
		}
		else
		{
			return esp;
		}
	}

	return NULL;
}

void __stdcall MerakSetUserParameter(
	__in Merak::Handle handle,
	__in void * param )
#pragma comment( linker, "/export:MerakSetUserParameter=_MerakSetUserParameter@8" )
{
	if ( NULL == handle )
	{
		return;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	object->userpram = param;
}

void * __stdcall MerakGetUserParameter( __in Merak::Handle handle )
#pragma comment( linker, "/export:MerakGetUserParameter=_MerakGetUserParameter@4" )
{
	if ( NULL == handle )
	{
		return NULL;
	}

	Merak::hook::HookObject * object = reinterpret_cast< Merak::hook::HookObject * >( handle );

	return object->userpram;
}