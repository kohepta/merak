﻿#pragma once

#include <stdint.h>

// ThreadId 型
struct ThreadId
{
	ThreadId() : val( 0 ) {}

	explicit ThreadId( __in long id )
		: val( id ) {}

	bool operator<( const ThreadId & other ) const
	{
		return this->val < other.val;
	}

	bool operator==( const ThreadId & other ) const
	{
		return this->val == other.val;
	}

	operator long() const
	{
		return val;
	}

	long val;
};

struct ThreadIdHashTraits
{
	static uint32_t hash( __in const ThreadId & x )
	{
		return x;
	}

	static bool equals(
		__in const ThreadId & x,
		__in const ThreadId & y )
	{
		return x == y;
	}
};