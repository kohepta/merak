﻿#include <tchar.h>
#include <stdio.h>
#include <windows.h>

#include <merak.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

static LPVOID __stdcall WNHVirtualAlloc(
	uintptr_t returaddress,
	Merak::Handle mrkHandle,
	LPVOID lpAddress,
	SIZE_T dwSize,
	DWORD flAllocationType,
	DWORD flProtect )
{
	//Native hook is not safety
	MerakEnterHookSafeArea();
	{
		printf( "called VirtualAlloc -- depth [%d]\n", MerakGetDepthOfCall() );
		printf( "    address:[0x%08x] size:[0x%08x]\n", lpAddress, dwSize );
	}
	MerakLeaveHookSafeArea();

	const void * real = MerakGetAlternativeFunction( mrkHandle );

	// call the real "__stdcall VirtualAlloc"
	return ( ( HANDLE ( __stdcall * )( LPVOID, SIZE_T, DWORD, DWORD ) )real )(
		lpAddress,
		dwSize,
		flAllocationType,
		flProtect );
}

static HANDLE __stdcall WHCreateFile(
	Merak::Handle mrkHandle,
	LPCTSTR lpFileName,                         // ファイル名
	DWORD dwDesiredAccess,                      // アクセスモード
	DWORD dwShareMode,                          // 共有モード
	LPSECURITY_ATTRIBUTES lpSecurityAttributes, // セキュリティ記述子
	DWORD dwCreationDisposition,                // 作成方法
	DWORD dwFlagsAndAttributes,                 // ファイル属性
	HANDLE hTemplateFile )                      // テンプレートファイルのハンドル
{
	printf( "called CreateFile -- depth [%d]\n", MerakGetDepthOfCall() );
	printf( "    fileName:[%s] \n", lpFileName );

	const void * targetFunction = MerakGetAlternativeFunction( mrkHandle );

	// call the real "__stdcall CreateFile"
	HANDLE retval = ( ( HANDLE ( __stdcall * )( LPCTSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE ) )targetFunction )(
		lpFileName,
		dwDesiredAccess,
		dwShareMode,
		lpSecurityAttributes,
		dwCreationDisposition,
		dwFlagsAndAttributes,
		hTemplateFile );

	SetLastError( 0x0000FFFF );

	return retval;
}

static int __cdecl WNHstrcmp(
	uintptr_t returaddress,
	Merak::Handle mrkHandle, 
	const char * s1, 
	const char * s2 )
{
	printf( "called memcpy -- depth [%d]\n", MerakGetDepthOfCall() );
	printf( "    s1:[0x%08x] s2:[0x%08x]\n", s1, s2 );

	const void * real = MerakGetAlternativeFunction( mrkHandle );

	// call the real "__cdecl strcmp"
	return ( ( int ( __cdecl * )( const char *, const char * ) )real )( s1, s2 );
}

static void * __cdecl WHmemcpy(
	uintptr_t reserved,
	Merak::Handle mrkHandle, 
	void * dst, 
	const void * src, 
	size_t n )
{
	printf( "called memcpy -- depth [%d]\n", MerakGetDepthOfCall() );
	printf( "    dst:[0x%08x] src:[0x%08x] n:[0x%08x]\n", dst, src, n );

	const void * targetFunction = MerakGetAlternativeFunction( mrkHandle );

	// call the real "__cdecl memcpy"
	void * retval = ( ( void * ( __cdecl * )( void *, const void *, size_t ) )targetFunction )( dst, src, n );

	SetLastError( 0x0000FFFF );

	return retval;
}

static bool __stdcall CommonPre( __in Merak::Handle mrkHandle )
{
	uintptr_t returnaddr = MerakGetOriginalEspReference( mrkHandle );
	const uint32_t * esp = MerakGetOriginalEspValue( mrkHandle );

	printf( "call [pre] %s --- depth [%d]\n", MerakGetOriginalFunctionName( mrkHandle ), MerakGetDepthOfCall() );
	printf( "    retaddr:[0x%08x] args 1:[0x%08x] 2:[0x%08x] 3:[0x%08x] 4:[0x%08x] 5:[0x%08x]\n",
		returnaddr,
		esp[1],
		esp[2],
		esp[3],
		esp[4],
		esp[5] );
	
	SetLastError( 0x0000FFFF );

	return true;
}

static void __stdcall CommonPost( uint32_t eax, __in const Merak::Handle mrkHandle )
{
	uintptr_t returnaddr = MerakGetOriginalEspReference( mrkHandle );
	const uint32_t * esp = MerakGetPreHookSavedArguments( mrkHandle );

	printf( "call [post] %s, retval:[0x%08x] retaddr:[0x%08x] --- depth [%d]\n", 
		MerakGetOriginalFunctionName( mrkHandle ), eax, returnaddr, MerakGetDepthOfCall());

	printf( "    retaddr:[0x%08x] args 1:[0x%08x] 2:[0x%08x] 3:[0x%08x] 4:[0x%08x] 5:[0x%08x]\n",
		returnaddr,
		esp[1],
		esp[2],
		esp[3],
		esp[4],
		esp[5] );

	SetLastError( 0x0000FFFF );

	return;
}

#pragma optimize( "", off )
static void Demonstration()
{
	int dst = 0;
	int src = 1000;

	LPVOID p = VirtualAlloc( NULL, 0x1000, MEM_COMMIT | MEM_RESERVE | MEM_TOP_DOWN, PAGE_EXECUTE_READWRITE );

	CreateFileA( "demo.txt", 0, 0, NULL, CREATE_ALWAYS, 0, NULL );

	printf( "CreateFileA - last error [0x%08x]\n", GetLastError() );

	memcpy( &dst, &src, sizeof( int ) );

	printf( "memcpy - last error [0x%08x]\n", GetLastError() );

	strcmp( (char *)&dst, (char *)&src );

	CreateEvent( NULL, TRUE, FALSE, NULL );

	printf( "CreateEvent - last error [0x%08x]\n", GetLastError() );

	VirtualFree( NULL, NULL, MEM_RELEASE );

	printf( "VirtualFree - last error [0x%08x]\n", GetLastError() );

	CreateSemaphore( NULL, 0, 0, NULL );

	printf( "CreateSemaphore - last error [0x%08x]\n", GetLastError() );

	return;
}
#pragma optimize( "", on )

int _tmain(int argc, _TCHAR* argv[])
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	{
		MerakInitialize();

		Merak::Handle mrk_handle1 = MerakInstallNativeWrapperHook( Merak::CC::__STDCALL, ::VirtualAlloc, WNHVirtualAlloc );
	
		if ( mrk_handle1 )
		{
			MerakAttachHook( mrk_handle1 );
		}

		Merak::Handle mrk_handle2 = MerakInstallWrapperHook( Merak::CC::__STDCALL, ::CreateFileA, WHCreateFile );
	
		if ( mrk_handle2 )
		{
			MerakAttachHook( mrk_handle2 );
		}

		Merak::Handle mrk_handle3 = MerakInstallNativeWrapperHook( Merak::CC::__CDECL, ::strcmp, WNHstrcmp );
	
		if ( mrk_handle3 )
		{
			MerakAttachHook( mrk_handle3 );
		}

		Merak::Handle mrk_handle4 = MerakInstallWrapperHook( Merak::CC::__CDECL, ::memcpy, WHmemcpy );
	
		if ( mrk_handle4 )
		{
			MerakAttachHook( mrk_handle4 );
		}

		Merak::Handle mrk_handle5 = MerakInstallPrePostHook( ::CreateEvent, "CreateEvent", CommonPre, CommonPost );
	
		if ( mrk_handle5 )
		{
			MerakAttachHook( mrk_handle5 );
		}

		Merak::Handle mrk_handle6 = MerakInstallPrePostHook( ::VirtualFree, "VirtualFree", CommonPre, CommonPost );
	
		if ( mrk_handle6 )
		{
			MerakAttachHook( mrk_handle6 );
		}

		Merak::Handle mrk_handle7 = MerakInstallPrePostHook( ::CreateSemaphore, "CreateSemaphore", CommonPre, NULL );
	
		if ( mrk_handle7 )
		{
			MerakAttachHook( mrk_handle7 );
		}

		Demonstration();

		MerakFinalize();
	}
	_CrtDumpMemoryLeaks();

	return 0;
}

