﻿#include "mrk_tls.h"
#include "mrk_lock.h"
#include "mrk_helper.h"
#include "mrk_substitute.h"
#include "mrk_container.h"
#include "mrk_internal_types.h"
#include "mrk_singleton.h"
#include "mrk_memory.h"
#include "mrk_defines.h"

namespace Merak { namespace tls { namespace
{
	using namespace Merak::helper;
	using namespace Merak::substitute;
	using namespace Merak::lock;
	using namespace Merak::container;

	class ThreadLocalStorage
		: private Noncopyable
	{
	private:
		struct CallState
		{
			CallState() : depth( 0 ), disablecnt( 0 ) {}
			CallState( uint32_t _depth, bool _disablecnt ) : depth( _depth ), disablecnt( _disablecnt ) {}

			uint32_t depth;
			uint32_t disablecnt;
		};
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
		struct LastErrorState
		{
			LastErrorState() : lastError( ERROR_SUCCESS ) {}
			explicit LastErrorState( long _lastError ) : lastError( _lastError ) {}

			long lastError;
		};
#endif
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		struct Arguments
		{
			operator uint32_t * ()
			{
				return arr;
			}

			uint32_t arr[(MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS + 1) * sizeof( uint32_t )];
		};

		typedef Stack< Arguments, Merak::memory::PoolAllocator< Arguments > >		ArgumentsStack;
#endif
		struct CallStateTable
		{
			CallStateTable() : instance(), spinWait( true ) {}

			typedef FixedHashTable< ThreadId, CallState, ThreadIdHashTraits, MERAK_NUMBER_OF_MANAGED_THREADS >		Instance;

			Instance instance;
			SpinWait spinWait;
		};

#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
		struct LastErrorStateTable
		{
			LastErrorStateTable() : instance(), spinWait() {}
			
			typedef FixedHashTable< ThreadId, LastErrorState, ThreadIdHashTraits, MERAK_NUMBER_OF_MANAGED_THREADS >	Instance;

			Instance instance;
			SpinWait spinWait;
		};
#endif
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		struct ArgumentsStackTable
		{
			ArgumentsStackTable() : instance(), spinWait() {}
			
			typedef FixedHashTable< ThreadId, ArgumentsStack, ThreadIdHashTraits, MERAK_NUMBER_OF_MANAGED_THREADS >	Instance;
			
			Instance instance;
			SpinWait spinWait;
		};
#endif
	public:
		bool isDisabledSlot( const ThreadId & tid )
		{
			ScopedLock lock( csTable_.spinWait );

			if ( disableAll_ )
			{
				return true;
			}

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				if ( 0 < it->second.disablecnt )
				{
					return true;
				}
			}

			return false;
		}

		void disableSlot( const ThreadId & tid )
		{
			ScopedLock lock( csTable_.spinWait );

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				it->second.disablecnt ++;
			}
			else
			{
				csTable_.instance.insert( tid, CallState( 0, 1 ) );
			}
		}

		void enableSlot( const ThreadId & tid )
		{
			ScopedLock lock( csTable_.spinWait );

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				if ( 0 < it->second.disablecnt )
				{
					it->second.disablecnt --;
				}

				if ( 0 == it->second.depth && 0 == it->second.disablecnt )
				{
					csTable_.instance.remove( tid );
				}
			}
		}

		void disableAllSlots()
		{
			disableAll_ = true;
		}

		void enableAllSlots()
		{
			disableAll_ = false;
		}
		
		uint32_t getDepthOfCall( const ThreadId & tid )
		{
			ScopedLock lock( csTable_.spinWait );

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				return it->second.depth;
			}

			return 0;
		}
		
		bool increaseDepthOfCall( const ThreadId & tid )
		{
			bool already = false;

			ScopedLock lock( csTable_.spinWait );

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				if ( 0 < it->second.depth )
				{
					already = true;
				}

				it->second.depth ++;
			}
			else
			{
				csTable_.instance.insert( tid, CallState( 1, 0 ) );
			}

			return already;
		}

		void decreaseDepthOfCall( const ThreadId & tid )
		{
			ScopedLock lock( csTable_.spinWait );

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				if ( 0 < it->second.depth )
				{
					it->second.depth --;
				}
				
				if ( 0 == it->second.depth && 0 == it->second.disablecnt )
				{
					csTable_.instance.remove( tid );
				}
			}
		}

		void clearDepthOfCall( const ThreadId & tid )
		{
			ScopedLock lock( csTable_.spinWait );

			CallStateTable::Instance::iterator it = csTable_.instance.find( tid );

			if ( it != csTable_.instance.end() )
			{
				it->second.depth = 0;
				
				if ( 0 == it->second.disablecnt )
				{
					csTable_.instance.remove( tid );
				}
			}
		}

#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
		void saveLastError( const ThreadId & tid )
		{
			uint32_t depth = getDepthOfCall( tid );
			long error = Merak::substitute::_GetLastError();

			ScopedLock lock( lesTable_.spinWait );

			LastErrorStateTable::Instance::iterator it = lesTable_.instance.find( tid );

			if ( it != lesTable_.instance.end() )
			{
				it->second.lastError = error;
			}
			else
			{
				lesTable_.instance.insert( tid, LastErrorState( error ) );
			}
		}

		void loadLastError( const ThreadId & tid )
		{
			ScopedLock lock( lesTable_.spinWait );

			LastErrorStateTable::Instance::iterator it = lesTable_.instance.find( tid );

			if ( it != lesTable_.instance.end() )
			{
				long error = it->second.lastError;

				Merak::substitute::_SetLastError( error );

				if ( getDepthOfCall( tid ) < 1 )
				{
					lesTable_.instance.remove( tid );
				}
			}
		}
#endif
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		void saveArguments(
			const ThreadId & tid,
			uint32_t esp )
		{
			ScopedLock lock( asTable_.spinWait );

			Arguments args;

			Merak::helper::Copy( args.arr, reinterpret_cast< uint8_t * >( esp ), sizeof( args.arr ) );

			ArgumentsStackTable::Instance::iterator it = asTable_.instance.find( tid );

			if ( it != asTable_.instance.end() )
			{
				it->second.push( args );
			}
			else
			{
				ArgumentsStack stack;

				stack.push( args );

				asTable_.instance.insert( tid, stack );
			}
		}

		const uint32_t * getArguments( const ThreadId & tid )
		{
			ScopedLock lock( asTable_.spinWait );

			ArgumentsStackTable::Instance::iterator it = asTable_.instance.find( tid );

			if ( it != asTable_.instance.end() )
			{
				return it->second.peek();
			}

			return NULL;
		}

		void clearArguments( const ThreadId & tid )
		{
			ScopedLock lock( asTable_.spinWait );

			ArgumentsStackTable::Instance::iterator it = asTable_.instance.find( tid );

			if ( it != asTable_.instance.end() )
			{
				it->second.pop();

				if ( it->second.empty() )
				{
					asTable_.instance.remove( tid );
				}
			}
		}
#endif
		static bool CreateInstance()
		{
			try
			{
				Merak::singleton::Singleton< ThreadLocalStorage >::Initialize();

				return true;
			}
			catch ( ... )
			{
			}

			return false;
		}

		static void DeleteInstance()
		{
			Merak::singleton::Singleton< ThreadLocalStorage >::Finalize();
		}

		static ThreadLocalStorage * GetInstance()
		{
			return Merak::singleton::Singleton< ThreadLocalStorage >::Get();
		}

	private:
		ThreadLocalStorage()
			: disableAll_( false ) 
			 ,csTable_()
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
			 ,lesTable_()
#endif
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
			 ,asTable_()
#endif
		{}

		~ThreadLocalStorage() {}

	private:
		Merak::atomic::type::AtomicBool disableAll_;
		CallStateTable csTable_;
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
		LastErrorStateTable lesTable_;
#endif
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		ArgumentsStackTable asTable_;
#endif
		friend Merak::singleton::Singleton< ThreadLocalStorage >;
	};

} } } // namespace Merak::tls::{unnamed}

namespace Merak { namespace tls
{
	bool Initialize()
	{
		return ThreadLocalStorage::CreateInstance();
	}

	void Finalize()
	{
		ThreadLocalStorage::DeleteInstance();
	}

	bool __stdcall IsDisabledSlot()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return false;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		return ThreadLocalStorage::GetInstance()->isDisabledSlot( threadid );
	}

	void __stdcall DisableSlot()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->disableSlot( threadid );
	}

	void __stdcall EnableSlot()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->enableSlot( threadid );
	}

	void __stdcall DisableAllSlots()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}
		
		ThreadLocalStorage::GetInstance()->disableAllSlots();
	}

	void __stdcall EnableAllSlots()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}
		
		ThreadLocalStorage::GetInstance()->enableAllSlots();
	}

	uint32_t __stdcall GetDepthOfCall()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return 0;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		return ThreadLocalStorage::GetInstance()->getDepthOfCall( threadid );
	}

	bool __stdcall IncreaseDepthOfCall()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return false;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		return ThreadLocalStorage::GetInstance()->increaseDepthOfCall( threadid );
	}

	void __stdcall DecreaseDepthOfCall()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->decreaseDepthOfCall( threadid );
	}

	void __stdcall ClearDepthOfCall()
	{
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}
		
		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->clearDepthOfCall( threadid );
	}

	void __stdcall SaveLastError()
	{
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->saveLastError( threadid );
#endif
	}

	void __stdcall LoadLastError()
	{
#ifdef MERAK_ENABLE_LAST_ERROR_PROTECTION
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->loadLastError( threadid );
#endif
	}

	void __stdcall SaveArguments( uint32_t esp )
	{
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->saveArguments( threadid, esp );
#endif
	}

	const uint32_t * __stdcall GetArguments()
	{
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return NULL;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		return ThreadLocalStorage::GetInstance()->getArguments( threadid );
#else
		return NULL;
#endif
	}

	void __stdcall ClearArguments()
	{
#if ( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 )
		if ( NULL == ThreadLocalStorage::GetInstance() )
		{
			return;
		}

		ThreadId threadid( Merak::substitute::_GetCurrentThreadId() );

		ThreadLocalStorage::GetInstance()->clearArguments( threadid );
#endif
	}

} } // namespace Merak::tls