﻿#pragma once

#include "mrk_internal_types.h"
#include "mrk_helper.h"
#include "mrk_tls.h"

namespace Merak { namespace wrapper
{
	template < typename Ret = void >
	class SafeInvoker
		: private Merak::helper::Noncopyable
	{
	public:
		template < typename F >
		static void Exec( F func )
		{
			Merak::tls::DisableSlot();
			func();
			Merak::tls::EnableSlot();
		}

		template < typename F, typename T1 >
		static void Exec( F func, T1 a1 )
		{
			Merak::tls::DisableSlot();
			func( a1 );
			Merak::tls::EnableSlot();
		}

		template < typename F, typename T1, typename T2 >
		static void Exec( F func, T1 a1, T2 a2 )
		{
			Merak::tls::DisableSlot();
			func( a1, a2 );
			Merak::tls::EnableSlot();
		}

		template < typename F, typename T1, typename T2, typename T3 >
		static void Exec( F func, T1 a1, T2 a2, T3 a3 )
		{
			Merak::tls::DisableSlot();
			func( a1, a2, a3 );
			Merak::tls::EnableSlot();
		}

		template < typename F, typename T1, typename T2, typename T3, typename T4 >
		static void Exec( F func, T1 a1, T2 a2, T3 a3, T4 a4 )
		{
			Merak::tls::DisableSlot();
			func( a1, a2, a3, a4 );
			Merak::tls::EnableSlot();
		}

		template < typename F, typename T1, typename T2, typename T3, typename T4, typename T5 >
		static void Exec( F func, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5 )
		{
			Merak::tls::DisableSlot();
			func( a1, a2, a3, a4, a5 );
			Merak::tls::EnableSlot();
		}

		template < typename F, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
		static void Exec( F func, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6 )
		{
			Merak::tls::DisableSlot();
			func( a1, a2, a3, a4, a5, a6 );
			Merak::tls::EnableSlot();
		}

		template < typename F >	
		static Ret Execr( F func )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r  = Merak::helper::force_cast< Ret >( func() );
			Merak::tls::EnableSlot();

			return r;
		}

		template < typename F, typename T1 >
		static Ret Execr( F func, T1 a1 )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r = Merak::helper::force_cast< Ret >( func( a1 ) );
			Merak::tls::EnableSlot();

			return r;
		}

		template < typename F, typename T1, typename T2 >
		static Ret Execr( F func, T1 a1, T2 a2 )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r  = Merak::helper::force_cast< Ret >( func( a1, a2 ) );
			Merak::tls::EnableSlot();

			return r;
		}

		template < typename F, typename T1, typename T2, typename T3 >
		static Ret Execr( F func, T1 a1, T2 a2, T3 a3 )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r  = Merak::helper::force_cast< Ret >( func( a1, a2, a3 ) );
			Merak::tls::EnableSlot();

			return r;
		}

		template < typename F, typename T1, typename T2, typename T3, typename T4 >
		static Ret Execr( F func, T1 a1, T2 a2, T3 a3, T4 a4 )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r = Merak::helper::force_cast< Ret >( func( a1, a2, a3, a4 ) );
			Merak::tls::EnableSlot();

			return r;
		}

		template < typename F, typename T1, typename T2, typename T3, typename T4, typename T5 >
		static Ret Execr( F func, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5 )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r =  Merak::helper::force_cast< Ret >( func( a1, a2, a3, a4, a5 ) );
			Merak::tls::EnableSlot();

			return r;
		}

		template < typename F, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
		static Ret Execr( F func, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6 )
		{
			Ret r;

			Merak::tls::DisableSlot();
			r =  Merak::helper::force_cast< Ret >( func( a1, a2, a3, a4, a5, a6 ) );
			Merak::tls::EnableSlot();

			return r;
		}

	private:
		SafeInvoker() {}
		~SafeInvoker() {}
	};

} } // namespace Merak::wrapper_caller
