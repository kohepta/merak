﻿#pragma once

#include "mrk_atomic.h"
#include "mrk_helper.h"

namespace Merak { namespace singleton
{
	template < typename T >
	class SingletonHelper
		: private Merak::helper::Noncopyable
	{
	public:
		template < typename Init >
		static void Initialize( Init init )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init );
		}

		template < typename Init, typename T1 >
		static void Initialize( Init init, T1 a1 )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init, a1 );
		}

		template < typename Init, typename T1, typename T2 >
		static void Initialize( Init init, T1 a1, T2 a2 )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init, a1, a2 );
		}

		template < typename Init, typename T1, typename T2, typename T3 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3 )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4 )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5 )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5 );
		}

		template < typename Init, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
		static void Initialize( Init init, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6 )
		{
			Merak::atomic::CallOnlyOnce( initFlag_, init, a1, a2, a3, a4, a5, a6 );
		}

		template < typename Fin >
		static void Finalize( Fin fin )
		{
			Merak::atomic::CallOnlyOnce( finFlag_, fin );
		}

	protected:
		SingletonHelper() {}

	private:
		static Merak::atomic::OnceFlag initFlag_;
		static Merak::atomic::OnceFlag finFlag_;
	};

	template < typename T >
	Merak::atomic::OnceFlag SingletonHelper< T >::initFlag_;

	template < typename T >
	Merak::atomic::OnceFlag SingletonHelper< T >::finFlag_;

	template < typename T >
	class Singleton
		: private Merak::helper::Noncopyable
	{
	public:
		static void Initialize()
		{
			SingletonHelper< T >::Initialize(
				[]()
				{
					instance_ = new T;
				});
		}

		template < typename T1 >
		static void Initialize( T1 a1 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1 )
				{
					instance_ = new T( a1 );
				},
				a1 );
		}

		template < typename T1, typename T2 >
		static void Initialize( T1 a1, T2 a2 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2 )
				{
					instance_ = new T( a1, a2 );
				},
				a1, a2 );
		}

		template < typename T1, typename T2, typename T3 >
		static void Initialize( T1 a1, T2 a2, T3 a3 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3 )
				{
					instance_ = new T( a1, a2, a3 );
				},
				a1, a2, a3 );
		}

		template < typename T1, typename T2, typename T3, typename T4 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T4 a4 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4 )
				{
					instance_ = new T( a1, a2, a3, a4 );
				},
				a1, a2, a3, a4 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5 )
				{
					instance_ = new T( a1, a2, a3, a4, a5 );
				},
				a1, a2, a3, a4, a5 );
		}

		template < typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
		static void Initialize( T1 a1, T2 a2, T3 a3, T5 a5, T6 a6 )
		{
			SingletonHelper< T >::Initialize(
				[]( T1 a1, T2 a2, T2 a3, T4 a4, T5 a5, T6 a6 )
				{
					instance_ = new T( a1, a2, a3, a4, a5, a6 );
				},
				a1, a2, a3, a4, a5, a6 );
		}

		static void Finalize()
		{
			SingletonHelper< T >::Finalize(
				[]()
				{
					if ( instance_ )
					{
						delete instance_;
						instance_ = NULL;
					}
				});
		}

		static T * Get()
		{
			return instance_;
		}

	protected:
		Singleton() {}

	private:
		static T * instance_;
	};

	template < typename T >
	T * Singleton< T >::instance_;
	
} }
