﻿#include <Windows.h>
#include <TlHelp32.h>
#include "mrk_thread.h"
#include "mrk_helper.h"
#include "mrk_lock.h"
#include "mrk_memory.h"
#include "mrk_container.h"
#include "mrk_substitute.h"
#include "mrk_singleton.h"
#include "mrk_defines.h"

namespace Merak { namespace thread { namespace
{
	using namespace Merak::helper;
	using namespace Merak::memory;
	using namespace Merak::substitute;
	using namespace Merak::lock;

	template < typename T >
	void EnumarationThreads(
		__in T fanctor,
		__in ThreadId tid = ThreadId( 0 ) )
	{
		ScopedHandle hSnapshot = ::CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, 0 );
		
		if ( INVALID_HANDLE_VALUE == hSnapshot )
		{
			return;
		}
		
		THREADENTRY32 te = { sizeof( te ) };
		
		if ( ::Thread32First( hSnapshot, &te ) )
		{
			do 
			{
				if ( ::GetCurrentProcessId() == te.th32OwnerProcessID
					&& GetCurrentThreadId() != te.th32ThreadID )
				{
					if ( 0 != tid )
					{
						if ( te.th32ThreadID == tid )
						{
							ScopedHandle hThread = 
								::OpenThread( THREAD_SUSPEND_RESUME, FALSE, te.th32ThreadID );

							fanctor( hThread );

							break;
						}
					}
					else
					{
						ScopedHandle hThread =
							::OpenThread( THREAD_SUSPEND_RESUME, FALSE, te.th32ThreadID );

						fanctor( hThread );
					}
				}
			} 
			while ( ::Thread32Next( hSnapshot, &te ) );
		}
	}

	struct Esp
	{
		Esp() : value( 0 ), reference( 0 ) {}
		Esp( __in uint32_t val, __in uintptr_t ref ) : value( val ), reference( ref ) {}

		uint32_t value;
		uintptr_t reference;
	};

	struct Context
	{
		Context() {}
		Context( __in uint32_t value, __in uintptr_t reference ) : esp( value, reference ) {}

		Esp esp; 
	};

	typedef Merak::container::Stack< Context, Merak::memory::PoolAllocator< Context > >		Contexts;

	class ThreadContext
		: private Noncopyable
	{
	private:
		typedef Merak::container::FixedHashTable< ThreadId, Contexts, ThreadIdHashTraits, MERAK_NUMBER_OF_MANAGED_THREADS >		ContextTable;

	public:
		void peek(
			__out uint32_t * value,
			__out uintptr_t * reference,
			__in size_t amount = 0 )
		{
			ThreadId tid( GetCurrentThreadId() );

			ScopedLock lock( spinWait_ );

			try
			{
				ContextTable::iterator it = contextTable_.find( tid );

				if ( it != contextTable_.end() )
				{
					Context ctx = it->second.peek( amount );

					if ( value )
					{
						*value = ctx.esp.value;
					}
				
					if ( reference )
					{
						*reference = ctx.esp.reference;
					}
				}
			}
			catch ( ... )
			{
			}
		}

		void push(
			__in uint32_t value,
			__in uintptr_t reference )
		{
			ThreadId tid( GetCurrentThreadId() );

			ScopedLock lock( spinWait_ );
			
			try
			{
				ContextTable::iterator it = contextTable_.find( tid );

				if ( it != contextTable_.end() )
				{
					it->second.push( Context( value, reference ) );
				}
				else
				{
					Contexts ctxs;

					ctxs.push( Context( value, reference ) );

					contextTable_.insert( tid, ctxs );
				}
			}
			catch ( ... )
			{
			}
		}

		void pop(
			__out uint32_t * value,
			__out uintptr_t * reference )
		{
			ThreadId tid( GetCurrentThreadId() );

			ScopedLock lock( spinWait_ );

			try
			{
				ContextTable::iterator it = contextTable_.find( tid );

				if ( it != contextTable_.end() )
				{
					Context ctx = it->second.peek();

					if ( value )
					{
						*value = ctx.esp.value;
					}
				
					if ( reference )
					{
						*reference = ctx.esp.reference;
					}

					it->second.pop();

					if ( it->second.empty() )
					{
						contextTable_.remove( tid );
					}
				}
			}
			catch ( ... )
			{
			}
		}

		static bool CreateInstance()
		{
			try
			{
				Merak::singleton::Singleton< ThreadContext >::Initialize();

				return true;
			}
			catch ( ... )
			{
			}

			return false;
		}

		static void DeleteInstance()
		{
			Merak::singleton::Singleton< ThreadContext >::Finalize();
		}

		static ThreadContext * GetInstance()
		{
			return Merak::singleton::Singleton< ThreadContext >::Get();
		}

	private:
		ThreadContext() : contextTable_(), spinWait_() {}
		~ThreadContext() {};
	
	private:
		ContextTable contextTable_;
		SpinWait spinWait_;
		
		friend Merak::singleton::Singleton< ThreadContext >;
	};
	
} } } // namespace Merak::thread::{unnamed}

namespace Merak { namespace thread
{
	bool Initialize()
	{
		return ThreadContext::CreateInstance();
	}

	void Finalize()
	{
		ThreadContext::DeleteInstance();
#if 0
		EnumarationThreads(
			[]( HANDLE handle ) { ::SuspendThread( handle ); },
			ThreadId( 0 ) );
#endif
	}

	void __stdcall PeekContext(
		__out uint32_t * pespval,
		__out uintptr_t * pespref,
		__in size_t amount /* = 0 */ )
	{
		if ( NULL == ThreadContext::GetInstance() )
		{
			return;
		}

		ThreadContext::GetInstance()->peek( pespval, pespref, amount );
	}

	void __stdcall PushContext(
		__in uint32_t espval,
		__in uintptr_t espref )
	{
		if ( NULL == ThreadContext::GetInstance() )
		{
			return;
		}

		ThreadContext::GetInstance()->push( espval, espref );
	}

	void __stdcall PopContext(
		__out uint32_t * pespval,
		__out uintptr_t * pespref )
	{
		if ( NULL == ThreadContext::GetInstance() )
		{
			return;
		}

		ThreadContext::GetInstance()->pop( pespval, pespref );
	}

} } // namespace Merak::thread