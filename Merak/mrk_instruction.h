#pragma once

#include "mrk_helper.h"
#include "mrk_wrapper.h"

namespace Merak { namespace instruction
{
	template < typename T >
	inline uintptr_t * Push(
		__in uint8_t * buffer,
		__in T data,
		__out uint32_t & pc )
	{
		uintptr_t * operand = NULL;
		uint32_t idx = 0;

		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0x68, idx );
		operand = reinterpret_cast< uintptr_t * >( &buffer[idx] );
		Merak::helper::WriteMemory< uint32_t >( &buffer[idx], data, idx );

		pc += idx;
		
		return operand;
	}

	inline void PushEsp(
		__in uint8_t * buffer,
		__in uint8_t offset,
		__out uint32_t & pc )
	{
		uint32_t idx = 0;

		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0xFF, idx );
		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0x74, idx );
		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0x24, idx );
		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], offset, idx );

		pc += idx;
	}

	template < typename T >
	inline uintptr_t * AbsJmp(
		__in uint8_t * buffer,
		__in T data,
		__in uintptr_t * relayTable,
		__out uint32_t & pc )
	{
		uintptr_t * operand = NULL;
		uint32_t idx = 0;

		Merak::helper::WriteMemory< uint16_t >( &buffer[idx], 0x25FF, idx );
		operand = reinterpret_cast< uintptr_t * >( &buffer[idx] );
		Merak::helper::WriteMemory< uint32_t >( &buffer[idx], relayTable, idx );
		Merak::helper::WriteMemory< uint32_t >( relayTable, data );

		pc += idx;
		
		return operand;
	}

	template < typename T >
	inline uintptr_t * AbsCall(
		__in uint8_t * buffer,
		__in T data,
		__in uintptr_t * relayTable,
		__out uint32_t & pc )
	{
		uintptr_t * operand = NULL;
		uint32_t idx = 0;

		Merak::helper::WriteMemory< uint16_t >( &buffer[idx], 0x15FF, idx );
		operand = reinterpret_cast< uintptr_t * >( &buffer[idx] );
		Merak::helper::WriteMemory< uint32_t >( &buffer[idx], relayTable, idx );
		Merak::helper::WriteMemory< uint32_t >( relayTable, data );

		pc += idx;
		
		return operand;
	}

	template < typename T >
	inline void MovToEsp(
		__in uint8_t * buffer,
		__in T data,
		__in uint8_t offset,
		__out uint32_t & pc )
	{
		uint32_t idx = 0;

		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0xC7, idx );
		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0x44, idx );
		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0x24, idx );
		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], offset, idx );
		Merak::helper::WriteMemory< uint32_t >( &buffer[idx], data, idx );

		pc += idx;
	}

	inline void Ret(
		__in uint8_t * buffer,
		__in uint16_t args,
		__out uint32_t & pc )
	{
		uint32_t idx = 0;

		Merak::helper::WriteMemory< uint8_t >( &buffer[idx], 0xC2, idx );
		Merak::helper::WriteMemory< uint16_t >( &buffer[idx], args, idx );

		pc += idx;
	}

	inline void Flush(
		__in void * code,
		__in size_t length )
	{
		::FlushInstructionCache( ::GetCurrentProcess(), code, length );
#if 0
		Merak::wrapper::SafeInvoker<>::Exec(
			::FlushInstructionCache, 
			Merak::wrapper::SafeInvoker< HANDLE >::Execr( ::GetCurrentProcess ), 
			code, 
			length );
#endif
	}

	class Protect
	{
	public:
		Protect()
			: executed_( false )
			  , code_( NULL )
			  , length_( 0 )
			  , oldprotect_( 0 ) {}

		~Protect()
		{
			if ( executed_ )
			{
				restore();
				Flush( code_, length_ );
			}
		}

		bool execute(
			__in void * code,
			__in size_t length,
			__in unsigned long protect )
		{
			code_ = code;
			length_ = length;

			executed_ = ::VirtualProtect( code, length, protect, &oldprotect_ ) ? true : false;

			return executed_;
		}
	
	private:
		void restore()
		{
			::VirtualProtect( code_, length_, oldprotect_, &oldprotect_ );
		}

	private:
		bool executed_;
		void * code_;
		size_t length_;
		unsigned long oldprotect_;
	};

} } // namespace Merak::instruction
