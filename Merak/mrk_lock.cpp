﻿#include "mrk_lock.h"
#include "mrk_substitute.h"
#include "mrk_wrapper.h"
#include "mrk_atomic.h"

namespace Merak { namespace lock
{
	void SpinWait::lock()
	{
		const uint32_t inc = ( 0 < kTimeout_ ) ? 1 : 0;

		for ( volatile uint32_t count = 0; ; )
		{
			if ( Merak::substitute::_GetCurrentThreadId() == impl_.currentval )
			{
				break;
			}

			if ( impl_.initval == Merak::atomic::CompareAndExchange(
				impl_.currentval, 
				impl_.initval, 
				impl_.newval ) )
			{
				impl_.currentval = Merak::substitute::_GetCurrentThreadId();
				break;
			}

			for ( ; kTimeout_ >= count && impl_.currentval != impl_.initval; count += inc )
			{
				if ( withoutThreadDispatch_ )
				{
					__asm rep nop	// cpu relax
				}
				else
				{
					Merak::wrapper::SafeInvoker<>::Exec( ::SwitchToThread );
				}
			}

			if ( kTimeout_ < count )
			{
				// unlock forcibly, because deadlock probably occurred.
				Merak::atomic::CompareAndExchange(
					impl_.currentval, 
					impl_.currentval,
					impl_.initval );
			}
		}
	}

	void SpinWait::unlock()
	{
		if ( Merak::substitute::_GetCurrentThreadId() == impl_.currentval )
		{
			Merak::atomic::CompareAndExchange(
				impl_.currentval, 
				Merak::substitute::_GetCurrentThreadId(),
				impl_.initval );
		}
	}

} } // namespace Merak::lock