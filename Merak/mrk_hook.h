﻿#pragma once

#include <stdint.h>
#include "merak.h"

namespace Merak { namespace hook
{
#pragma pack( push, 1 )
	struct ShortJmp
	{
		uint8_t opcode;
		uint8_t operand;
	};

	struct RelJmp
	{
		uint8_t opcode;
		union
		{
			uint32_t operand;
			uint8_t bytes[sizeof( uint32_t )];
		};
	};
	typedef RelJmp RelCall;

	struct AbsJmp
	{
		uint16_t opcode;
		union
		{
			uint32_t operand;
			uint8_t bytes[sizeof( uint32_t )];
		};
	};
	typedef AbsJmp AbsCall, RelJcc;

	struct AbsJcc
	{
		uint8_t opcode;		// jcc 
		uint8_t fix_operand;
		ShortJmp shortJmp;
		uint16_t fix_opcode;
		uint32_t operand;
	};

	union CodeBytes
	{
		uint32_t value;
		uint8_t bytes[sizeof( uint32_t )];
	};
#pragma pack( pop )

	struct HookType
	{
		enum type
		{
			NATIVE_WRAPPER,
			WRAPPER,
			PRE,
			PREPOST
		};
	};

	struct AttachStyle
	{
		enum type
		{
			NONE,
			RELJMP,
			HOTPATCH_NOP,
			HOTPATCH_INT3
		};
	};

	// 関数プロローグ長
	struct PrologLength
	{
		PrologLength() : original( 0 ), relayTable( 0 ), preAlternative( 0 ), alternative( 0 ), replacement( 0 ) {}
		
		operator size_t() const
		{
			return original + relayTable + preAlternative + alternative;
		}

		bool valid() const
		{
			return this->operator size_t() > 0 && replacement >= sizeof( RelJmp );
		}

		size_t original;				// 書き換え対象のプロローグ長
		size_t relayTable;
		size_t preAlternative;
		size_t alternative;				// 書き換えられた関数プロローグの直後に戻る為のコード長
										// オリジナルプロローグ長 (アドレス解決済)
										//   (+ 関数プロローグの直後に戻る jmp コード長)
		size_t replacement;
	};

	struct PrologObject
	{
		PrologLength length;
#pragma warning( push )
#pragma warning( disable:4200 )
		uint8_t bytes[];				// コード領域先頭 (Flexible array member)
#pragma warning( pop )
	};

	// フックオブジェクト
	//
	// コード領域先頭からスタブコード長分フック
	// を実現する為に必要なインストラクションが格納される
	struct HookObject
	{
		bool attached;					// アタッチ済フラグ
		HookType::type hookType;
		AttachStyle::type attachStyle;
		void * userpram;				// ユーザーパラメータ
		void * targetFunction;			// フック対象関数アドレス
		char targetFunctionName[64];	// フック対象関数名
		
		PrologObject * prolog;
		
		struct BridgeObject
		{
			size_t relayTableLength;	// 中継テーブル長
			size_t length;				// スタブコード長
#pragma warning( push )
#pragma warning( disable:4200 )
			uint8_t bytes[];			// コード領域先頭 (Flexible array member)
#pragma warning( pop )
		}
		bridge;
	};

	void * GetAlternativeFunction( __in const HookObject * object );

	// 関数プロローグ長を取得
	PrologLength GetPrologLength(
		__in const void * targetFunction,
		__in const size_t replacement );

	HookObject * CreateNativeWrapperHook(
		__in CC::type cc,
		__in const void * targetFunction,
		__in const void * wrapperFunction,
		__in const PrologLength & prologLength,
		__in bool force = false );

	// 指定関数に対するラッパーフックを生成
	HookObject * CreateWrapperHook(
		__in CC::type cc,
		__in const void * targetFunction,
		__in const void * wrapperFunction,
		__in const PrologLength & prologLength,
		__in bool force = false );

	// 指定関数に対する事前事後処理フックを生成
	HookObject * CreatePrePostHook(
		__in const void * targetFunction,
		__in const void * preFunction,
		__in const void * postFunction,
		__in const PrologLength & prologLength,
		__in bool force = false,
		__in uint16_t retArgSize = 0 );
	
	// フックオブジェクト削除
	void DeleteHookObject( __in HookObject * object );

	// 関数プロローグ部をフックにジャンプするコードに置きかえる
	bool ReplaceProlog( __in HookObject * object );

	// 関数プロローグ部を書き換えれる前の状態に戻す
	bool RestoreProlog( __in const HookObject * object );

} } // namespace Merak::hook