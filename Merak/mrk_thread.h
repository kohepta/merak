﻿#pragma once

#include "mrk_internal_types.h"
#include <stdint.h>

namespace Merak { namespace thread
{
	bool Initialize();

	void Finalize();

	void __stdcall PeekContext(
		__out uint32_t * pespval,
		__out uintptr_t * pespref,
		__in size_t amount = 0 );

	void __stdcall PushContext(
		__in uint32_t espval,
		__in uintptr_t espref );

	void __stdcall PopContext(
		__out uint32_t * pespval,
		__out uintptr_t * pespref );

} } // namespace Merak::thread