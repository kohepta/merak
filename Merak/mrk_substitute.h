﻿#pragma once

namespace Merak { namespace substitute
{
	// GetCurrentThreadId の代替
	// Merak は GetCurrentThreadId ではなくこちらを使用する
	// これも既に GetCurrentThreadId がフックされた場合に備えたもの
	long __stdcall _GetCurrentThreadId();

	long __stdcall _InterlockedCompareExchange(
		long volatile * destination,	// 操作対象の値へのポインタへのポインタ
		long exchange,					// 入れ替える値
		long comparand );				// 比較対象の値

	void * __stdcall _InterlockedCompareExchangePointer(
		void * volatile * destination,
		void * exchange,
		void * comperand );

	long __stdcall _GetLastError();

	void __stdcall _SetLastError( long lasterror );

} } // namesapce Merak::substitute