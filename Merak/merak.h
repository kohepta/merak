﻿#pragma once

#include <stdint.h>

#ifndef MERAK_DECLSPEC
    #ifdef MERAK_EXPORTS
    #define MERAK_DECLSPEC		extern "C" __declspec( dllexport )
    #else
    #define MERAK_DECLSPEC		extern "C" __declspec( dllimport )
    #endif
#endif

namespace Merak
{
	typedef void * Handle;
	typedef bool ( __stdcall * PreHookFunction )( __in const Handle handle ) ;
	typedef void ( __stdcall * PostHookFunction )( __in uint32_t eax, __in const Handle handle );

	struct CC
	{
		enum type
		{
			__STDCALL,
			__CDECL
		};
	};
}

////////////////////////////////////////////////////////////////////////////////
//
// prototypes
//

MERAK_DECLSPEC bool __stdcall MerakInitialize();

MERAK_DECLSPEC void __stdcall MerakFinalize();

/*！
 * \fn			MerakInstallNativeWrapperHook
 *
 * \brief		ネイティブラッパー型フックインストール
 *
 * \note		ネイティブラッパーはフックが再帰的に呼ばれることを
 *				MerakEnterHookSafeArea()、MerakLeaveHookSafeArea() で明示的に防ぐ必要がある。
 *
 * \param[in]	cc						呼び出し規約 (Merak::CC::__STDCALL or Merak::CC::__CDECL)
 * \param[in]	targetFunction			フック対象関数
 * \param[in]	wrapperFunction			フック関数
 * \param[in]	force					強制フックフラグ
 * \return		ハンドル
 */
MERAK_DECLSPEC Merak::Handle __stdcall MerakInstallNativeWrapperHook(
	__in Merak::CC::type cc,
	__in const void * targetFunction,
	__in const void * wrapperFunction,
	__in bool force = false );

/*！
 * \fn			MerakInstallWrapperHook
 *
 * \brief		ラッパー型フックインストール
 *
 * \param[in]	cc						呼び出し規約 (Merak::CC::__STDCALL or Merak::CC::__CDECL)
 * \param[in]	wrapperFunction			フック関数
 * \param[in]	wrapperFunction			フック関数
 * \param[in]	force					強制フックフラグ
 * \return		ハンドル
 */
MERAK_DECLSPEC Merak::Handle __stdcall MerakInstallWrapperHook(
	__in Merak::CC::type cc,
	__in const void * targetFunction,
	__in const void * wrapperFunction,
	__in bool force = false );

/*！
 * \fn			MerakInstallPrePostHook
 *
 * \brief		事前・事後処理型フックインストール
 *
 * \param[in]	targetFunction			フック対象関数
 * \param[in]	targetFunctionName		フック対象関数名
 * \param[in]	preFunction				事前フック関数
 * \param[in]	postFunction			事後フック関数
 * \param[in]	force					強制フックフラグ
 * \param[in]	retArgsSize				フック対象関数が持つ引数の合計サイズ (通常は指定しない)
 * \return		ハンドル
 */
MERAK_DECLSPEC Merak::Handle __stdcall MerakInstallPrePostHook(
	__in const void * targetFunction,
	__in const char * targetFunctionName,
	__in const Merak::PreHookFunction preFunction,
	__in const Merak::PostHookFunction postFunction,
	__in bool force = false,
	__in uint16_t retArgSize = 0 );

/*！
 * \fn			MerakUninstallHook
 *
 * \brief		フックアンインストール
 *
 * \param[in]	handle					ハンドル
 */
MERAK_DECLSPEC void __stdcall MerakUninstallHook( __in Merak::Handle handle );

/*！
 * \fn			MerakAttachHook
 *
 * \brief		インストール済フックアタッチ
 *
 * \param[in]	handle					ハンドル
 * \return		true					成功
 * \return		false					失敗
 */
MERAK_DECLSPEC bool __stdcall MerakAttachHook( __in Merak::Handle handle );

/*！
 * \fn			MerakDetachHook
 *
 * \brief		インストール済フックデタッチ
 *
 * \param[in]	handle					ハンドル
 * \return		true					成功
 * \return		false					失敗
 */
MERAK_DECLSPEC void __stdcall MerakDetachHook( __in Merak::Handle handle );

/*！
 * \fn			MerakEnterHookSafeArea
 *
 * \brief		関数ロック取得 (自スレッドのみ)
 */
MERAK_DECLSPEC void __stdcall MerakEnterHookSafeArea();
	
/*！
 * \fn			MerakLeaveHookSafeArea
 *
 * \brief		関数ロック解放 (自スレッドのみ)
 */
MERAK_DECLSPEC void __stdcall MerakLeaveHookSafeArea();

/*！
 * \fn			MerakSuspend
 *
 * \brief		関数ロック取得 (全スレッド)
 */
MERAK_DECLSPEC void __stdcall MerakSuspend();
	
/*！
 * \fn			MerakResume
 *
 * \brief		関数ロック解放 (全スレッド)
 */
MERAK_DECLSPEC void __stdcall MerakResume();

/*！
 * \fn			MerakGetDepthOfCall
 *
 * \brief		関数フックの深さを取得
 *
 * \return		関数フックの深さ
 */
MERAK_DECLSPEC uint32_t __stdcall MerakGetDepthOfCall();

/*！
 * \fn			MerakIncreaseDepthOfCall
 *
 * \brief		関数フックの深さを加算
 */
MERAK_DECLSPEC bool __stdcall MerakIncreaseDepthOfCall();

/*！
 * \fn			MerakDecreaseDepthOfCall
 *
 * \brief		関数フックの深さを減算
 */
MERAK_DECLSPEC void __stdcall MerakDecreaseDepthOfCall();

/*！
 * \fn			MerakClearDepthOfCall
 *
 * \brief		関数フックの深さをクリア
 *
 */
MERAK_DECLSPEC void __stdcall MerakClearDepthOfCall();

 /*！
  * \fn			MerakGetAlternativeFunction
  *
  * \brief		実物の関数アドレスを取得 (ラッパー型フック関数内で使用)
  *
  * \param[in]	handle					ハンドル
  * \return		
  */
MERAK_DECLSPEC void * __stdcall MerakGetAlternativeFunction( __in const Merak::Handle handle );

/*！
 * \fn			MerakGetOriginalFunction
 *
 * \brief		フックされたフック対象関数アドレスを取得
 *
 * \note		この関数で取得したアドレスは情報としての価値しかなく、本物の関数として呼び出すことはできない。
 *				なぜなら、この関数で取得した関数のプロローグ部分は既にフックコードで置き換わっている為、呼び出
 *				しても再び同じフック関数に戻ってきてしまう。
 *				ラッパー型フック関数内で実物の関数を呼び出す場合は、MerakGetAlternativeFunction
 *				で取得したアドレスに対して行う。
 *
 * \param[in]	handle					ハンドル
 * \return		フック対象関数アドレス
 */
MERAK_DECLSPEC const void * __stdcall MerakGetOriginalFunction( __in const Merak::Handle handle );

/*！
 * \fn			MerakGetOriginalFunctionName
 *
 * \brief		フック対象関数名を取得
 *
 * \param[in]	handle					ハンドル
 * \return		フック対象関数名
 */
MERAK_DECLSPEC const char * __stdcall MerakGetOriginalFunctionName( __in const Merak::Handle handle );

/*！
 * \fn			MerakGetOriginalEspValue
 *
 * \brief		esp の値を取得
 *
 * \param[in]	handle					ハンドル
 * \return		
 */
MERAK_DECLSPEC const uint32_t * __stdcall MerakGetOriginalEspValue( __in const Merak::Handle handle );

/*！
 * \fn			MerakGetOriginalEspReference
 *
 * \brief		esp の参照先を取得
 *
 * \param[in]	handle					ハンドル
 * \return		
 */
MERAK_DECLSPEC uintptr_t __stdcall MerakGetOriginalEspReference( __in const Merak::Handle handle );

/*！
 * \fn			MerakGetPreHookSavedArguments
 *
 * \brief		saved esp の値を取得
 *
 * \note		post hook で呼ぶ
 *				( MERAK_SAVED_NUMBER_OF_PREHOOK_ARGUMENTS > 0 ) が定義されている場合のみ有効
 *
 * \param[in]	handle					ハンドル
 * \return		
 */
MERAK_DECLSPEC const uint32_t * __stdcall MerakGetPreHookSavedArguments( __in const Merak::Handle handle );

/*！
 * \fn			MerakSetUserParameter
 *
 * \brief		ハンドルに任意のユーザーパラメータを設定
 *
 * \param[in]	handle					ハンドル
 * \param[in]	param					ユーザーパラメータ
 * \return		
 */
MERAK_DECLSPEC void __stdcall MerakSetUserParameter(
	__in Merak::Handle handle,
	__in void * param );

/*！
 * \fn			MerakGetUserParameter
 *
 * \brief		ハンドルから設定済のユーザーパラメータを設定
 *
 * \param[in]	handle					ハンドル
 * \return		ユーザーパラメータ
 */
MERAK_DECLSPEC void * __stdcall MerakGetUserParameter( __in Merak::Handle handle );