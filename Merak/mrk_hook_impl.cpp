﻿#include <algorithm>
#include "mrk_hook_impl.h"
#include "mrk_memory.h"
#include "mrk_stub.h"
#include "mrk_thread.h"

namespace Merak { namespace hook { namespace impl
{
	PrologObject * CreatePrologObject( __in const PrologLength & prologLength )
	{
		PrologObject * prolog = reinterpret_cast< PrologObject * >( 
			memory::AllocatePool( sizeof( PrologObject ) + prologLength ) );

		if ( NULL == prolog )
		{
			return NULL;
		}

		prolog->length = prologLength;

		for ( size_t i = 0 ; i < prolog->length; i ++ )
		{
			prolog->bytes[i] = 0x90;	// nop
		}

		return prolog;
	}

	HookObject * CreateHookObject(
		__in const void * targetFunction,
		__in const PrologLength & prologLength,
		__in uint8_t relayTableIndex,
		__in uint8_t codeLength )
	{
		size_t relayTableLength = relayTableIndex * sizeof( uintptr_t );
		size_t overall = sizeof( HookObject ) + relayTableLength + codeLength;
		HookObject * object = reinterpret_cast< HookObject * >( memory::AllocatePool( overall ) );

		if ( NULL == object )
		{
			return NULL;
		}
		
		PrologObject * prolog = CreatePrologObject( prologLength );

		if ( NULL == prolog )
		{
			memory::ReleasePool( object );

			return NULL;
		}

		object->targetFunction = const_cast< void * >( targetFunction );
		object->prolog = prolog;
		object->bridge.relayTableLength = relayTableLength;
		object->bridge.length = codeLength;

		for ( size_t i = 0 ; i < relayTableLength + codeLength; i ++ )
		{
			object->bridge.bytes[i] = 0x90;	// nop
		}

		return object;
	}

	bool SelectAttachStyle( __in HookObject * object )
	{
		bool success = true;
		static const uint8_t nop_field[] = { 0x90, 0x90, 0x90, 0x90, 0x90 };
		static const uint8_t int3_field[] = { 0xCC, 0xCC, 0xCC, 0xCC, 0xCC };
		const uint8_t * target8 = reinterpret_cast< const uint8_t * >( object->targetFunction );

		object->attachStyle = AttachStyle::NONE;

		if ( object->prolog->length.original < sizeof( RelJmp ) && 0xEB == *target8 )
		{
			if ( 0 == helper::Compare( target8 - sizeof( nop_field ), nop_field, sizeof( nop_field ) ) )
			{
				object->attachStyle = AttachStyle::HOTPATCH_NOP;
			}
			else if ( 0 == helper::Compare( target8 - sizeof( int3_field ), int3_field, sizeof( int3_field ) ) )
			{
				object->attachStyle = AttachStyle::HOTPATCH_INT3;
			}
			else
			{
				success = false;
			}
		}
		else
		{
			switch ( object->prolog->length.replacement )
			{
			case sizeof( RelJmp ):
				object->attachStyle = AttachStyle::RELJMP;
				break;
			default:
				success = false;
				break;
			}
		}

		return success;
	}

	AlternativeProlog::Builder::Builder( __in PrologObject * object )
		: prologObject_( object ), relayTableIndex_( 0 ), fixed_( object->length.valid() )
	{
	}

	uintptr_t AlternativeProlog::Builder::makeRelayTable( __in uintptr_t dest )
	{
		uintptr_t * table = GetAlternativeRelayTable( prologObject_ );

		uintptr_t address = reinterpret_cast< uintptr_t >( &table[relayTableIndex_] );

		if ( fixed_ )
		{
			table[relayTableIndex_] = dest;
		}

		relayTableIndex_ ++;
			
		return address;
	}

	void AlternativeProlog::Builder::makePreAlternative()
	{
		if ( fixed_ && 0 < prologObject_->length.preAlternative )
		{
			uint8_t * preAlternative = GetPreAlternativeProlog( prologObject_ );
			uint32_t * pushContext = reinterpret_cast< uint32_t * >(
				&preAlternative[Merak::stub::PreAlternative::GetOffsetOfPushContext()] );
			uint32_t * postAlternative = reinterpret_cast< uint32_t * >(
				&preAlternative[Merak::stub::PreAlternative::GetOffsetOfPostAlternative()] );

			Merak::helper::Copy( 
				preAlternative, 
				Merak::stub::PreAlternative::GetCode(), 
				prologObject_->length.preAlternative );

			*pushContext = reinterpret_cast< uint32_t >( Merak::thread::PushContext );
			*postAlternative = reinterpret_cast< uint32_t >( Merak::stub::PostAlternative );
		}
	}

	void AlternativeProlog::Builder::copyCode(
		__in uintptr_t position,
		__in const void * code,
		__in size_t codeSize )
	{
		if ( fixed_ )
		{
			uint8_t * buffer = reinterpret_cast< uint8_t * >( GetAlternativeProlog( prologObject_ ) );

			Merak::helper::Copy( &buffer[position], code, codeSize );
		}
	}

	void AlternativeProlog::Builder::fixOriginalPrologLength( __in size_t size )
	{
		if ( !fixed_ )
		{
			prologObject_->length.original = size;
		}
	}

	void AlternativeProlog::Builder::fixAlternativePrologLength( __in size_t size )
	{
		if ( !fixed_ )
		{
			prologObject_->length.alternative = size;
		}
	}

	void AlternativeProlog::Builder::fixRalayTableLength()
	{
		if ( !fixed_ )
		{
			prologObject_->length.relayTable = relayTableIndex_ * sizeof( uintptr_t );
		}
	}

	void AlternativeProlog::Builder::fixReplacementLegnth( __in size_t size )
	{
		if ( !fixed_ )
		{
			prologObject_->length.replacement = size;
		}
	}
	
	AlternativeProlog::AlternativeProlog( __in const void * targetFunction )
		: target_( targetFunction )
	{
	}

	PrologLength AlternativeProlog::getLength( __in size_t replacement )
	{
		PrologObject object;

		if ( NULL == target_ || !disassemble( replacement, &object ) )
		{
			return PrologLength();
		}

		return object.length;
	}

	bool AlternativeProlog::build( __out PrologObject * object )
	{
		if ( !object )
		{
			return false;
		}

		return disassemble( object->length.replacement, object );
	}

	bool AlternativeProlog::disassemble(
		__in size_t replacement,
		__out PrologObject * object )
	{
		AbsCall call = { 0x15FF, 0x00000000 };
		AbsJmp jmp = { 0x25FF, 0x00000000  };
		AbsJcc jcc = { 0x70, 0x02, 0xEB, sizeof( jmp ), jmp.opcode, jmp.operand };
		
		size_t orgpos = 0;
		size_t altpos = 0;
		uintptr_t jmpdest = 0;

		AlternativeProlog::Builder builder( object );

		for ( bool loop = true; loop; ) 
		{
			HDEObject hdeObj;
			const uint8_t * inst = reinterpret_cast< const uint8_t * >( target_ ) + orgpos;
			
			HDEDisassemble( inst, hdeObj );

			if ( F_ERROR & hdeObj.flags )
			{
				return false; 
			}

			const void * lastinst = inst;
			size_t lastinstlen = hdeObj.len;

			if ( static_cast< size_t >( inst - reinterpret_cast< const uint8_t * >( target_ ) ) >= replacement )
			{
				// 走査終了
				uintptr_t dest = reinterpret_cast< uintptr_t >( inst );
				jmp.operand = builder.makeRelayTable( dest );
				lastinst = &jmp;
				lastinstlen = sizeof( jmp );				
				loop = false;
			}
			else 
			{
				// 相対直接 call
				if ( 0xE8 == hdeObj.opcode )
				{
					uintptr_t dest = getAbsoluteDestinationAddress( hdeObj, inst );
					call.operand = builder.makeRelayTable( dest );
					lastinst = &call;
					lastinstlen = sizeof( call );
				}
				// 相対直接 jmp ( EB or E9 )
				else if ( 0xE9 == ( hdeObj.opcode & 0xFD ) )
				{
					uintptr_t dest = getAbsoluteDestinationAddress( hdeObj, inst );

					if ( insideJump( target_, dest, replacement ) )
					{
						jmpdest = std::max< uintptr_t >( jmpdest, dest ); 
					}
					else
					{
						jmp.operand = builder.makeRelayTable( dest );
						lastinst = &jmp;
						lastinstlen = sizeof( jmp );
						loop = isJumping( inst, jmpdest );
					}
				}
				// 相対直接 jcc
				else if ( 0x70 == ( hdeObj.opcode & 0xF0 )
					|| 0x80 == ( hdeObj.opcode2 & 0xF0 )
					|| 0xE3 == hdeObj.opcode )
				{
					uintptr_t dest = getAbsoluteDestinationAddress( hdeObj, inst );

					if ( insideJump( target_, dest, replacement ) ) 
					{ 
						jmpdest = std::max< uintptr_t >( jmpdest, dest ); 
					}
					else if ( 0xE3 == hdeObj.opcode )
					{
						// failed
						return false; 
					}
					else
					{
						jcc.operand = builder.makeRelayTable( dest );
						setJccOpcode( hdeObj, jcc );
						lastinst = &jcc;
						lastinstlen = sizeof( jcc );
					}
				}
				// リターン ret
				else if ( 0xC2 == ( hdeObj.opcode & 0xFE ) ) 
				{ 
					loop = isJumping( inst, jmpdest ); 
				}

				orgpos += hdeObj.len;
			}

			if ( isJumping( inst, jmpdest ) && lastinstlen != hdeObj.len )
			{
				// failed 
				return false; 
			}

			builder.copyCode( altpos, lastinst, lastinstlen );
			altpos += lastinstlen;
		}

		builder.makePreAlternative();
		builder.fixOriginalPrologLength( orgpos );
		builder.fixAlternativePrologLength( altpos );
		builder.fixRalayTableLength();
		builder.fixReplacementLegnth( replacement );

		// success
		return true;
	}

	bool AlternativeProlog::insideJump(
		__in const void * targetFunction, 
		__in uintptr_t destination,
		__in size_t replacement )
	{
		uintptr_t p = reinterpret_cast< uintptr_t >( targetFunction );
#if 0
		return ( p <= destination && destination <= p + replacement );
#else
		return ( p <= destination && destination < p + replacement );
#endif
	}

	bool AlternativeProlog::isJumping(
		__in const void * pc,
		__in uintptr_t destination )
	{
		return reinterpret_cast< uintptr_t >( pc ) < destination;
	}

	void AlternativeProlog::setJccOpcode(
		__in const HDEObject & hdeObj,
		__out RelJcc & jcc )
	{
		uint8_t code = ( ( hdeObj.opcode != 0x0F ? hdeObj.opcode : hdeObj.opcode2 ) & 0x0F );
		jcc.opcode = 0x800F | ( code << 8 );
	}

	void AlternativeProlog::setJccOpcode(
		__in const HDEObject & hdeObj,
		__out AbsJcc & jcc )
	{
		uint8_t code = ( ( hdeObj.opcode != 0x0F ? hdeObj.opcode : hdeObj.opcode2 ) & 0x0F );
		jcc.opcode = 0x70 | code;
	}

	uintptr_t AlternativeProlog::getAbsoluteDestinationAddress(
		__in const HDEObject & hdeObj,
		__in const uint8_t * inst )
	{
		int32_t disp = 0;

		if ( F_IMM8 & hdeObj.flags )
		{
			disp = static_cast< int8_t >( hdeObj.imm.imm8 );
		}
		else if ( F_IMM16 & hdeObj.flags )
		{
			disp = static_cast< int16_t >( hdeObj.imm.imm16 );
		}
		else
		{
			disp = static_cast< int32_t >( hdeObj.imm.imm32 );
		}
		
		return reinterpret_cast< uintptr_t >( inst ) + hdeObj.len + disp;
	}

} } } // namespace Merak::hook::impl