﻿#include "mrk_substitute.h"

namespace Merak { namespace substitute
{
#pragma optimize( "", off )
#pragma runtime_checks( "", off )
#pragma check_stack( off )

	__declspec( naked ) long __stdcall _GetCurrentThreadId()
	{
		__asm
		{
			mov		eax, dword ptr fs:[24]
			mov		eax, dword ptr [eax+36]
			retn
		}
	}

	__declspec( naked ) long __stdcall _InterlockedCompareExchange(
		long volatile * destination,
		long exchange,
		long comparand )
	{
		__asm
		{
			mov		ecx, dword ptr [esp+4]
			mov		edx, dword ptr [esp+8]
			mov		eax, dword ptr [esp+12]
			lock cmpxchg dword ptr [ecx], edx
			retn	12
		}
	}

	void * __stdcall _InterlockedCompareExchangePointer(
		void * volatile * destination,
		void * exchange,
		void * comperand )
	{
	    return( (void *)(long __w64)_InterlockedCompareExchange(
			(long volatile *)destination, 
			(long)(long __w64)exchange, 
			(long)(long __w64)comperand ) );
	}

	__declspec( naked ) long __stdcall _GetLastError()
	{
		__asm
		{
			mov		eax, dword ptr fs:[24]
			mov		eax, [eax+52]
			retn
		}
	}

	__declspec( naked ) void __stdcall _SetLastError( long lasterror )
	{
		__asm
		{
			mov     eax, [esp+4]		// arg: last error
			mov		ecx, dword ptr fs:[24]
			mov		[ecx+52], eax
			retn	4
		}
	}

#pragma optimize( "", on )
#pragma runtime_checks( "", restore )
#pragma check_stack( on )

} } // namesapce Merak::substitute