﻿#pragma once

#include <stdint.h>

namespace Merak { namespace stub
{
	class PreAlternative
	{
	public:
		static const uint8_t * GetCode();
		static const size_t GetOffsetOfPushContext();
		static const size_t GetOffsetOfPostAlternative();
		static const size_t GetCodeSize();

	private:
		static const uint8_t kCode_[];
	};

	void __stdcall Check();

	void __stdcall PreStub();

	void __stdcall PostStub();

	void __stdcall PostAlternative();

	void __stdcall EnterHandlerForCDECLWrapper();

	void __stdcall EnterHandlerForPre();

	void __stdcall EnterHandlerForPrePost();

	void __stdcall LeaveHandlerForPrePost();

} } // namespace Merak::stub