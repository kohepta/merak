﻿#include <Windows.h>
#include "mrk_stub.h"
#include "mrk_thread.h"
#include "mrk_tls.h"
#include "mrk_hook.h"

namespace Merak { namespace stub
{
#pragma optimize( "", off )
#pragma runtime_checks( "", off )
#pragma check_stack( off )

	const uint8_t PreAlternative::kCode_[] =
	{
		0x9c,								// pushfd
		0x60,								// pushad
		0x55,								// push	 ebp
		0x8b, 0xec,							// mov	 ebp, esp
		0x8d, 0x45, 0x28,					// lea	 eax, dword ptr [ebp+40]
		0xff, 0x30,							// push	 dword ptr [eax]
		0x50,								// push	 eax
		0xb8, 0x00, 0x00, 0x00, 0x00,		// mov	 eax, Merak::thread::PushContext
		0xff, 0xd0,							// call	 eax
		0xb8, 0x00, 0x00, 0x00, 0x00,		// mov	 eax, Merak::stub::PostAlternative
		0x89, 0x45, 0x28,					// mov	 dword ptr [ebp+40], eax
		0x8b, 0xe5,							// mov	 esp, ebp
		0x5d,								// pop	 ebp
		0x61,								// popad
		0x9d								// popfd
	};

	const uint8_t * PreAlternative::GetCode()
	{
		return kCode_;
	}
	
	const size_t PreAlternative::GetOffsetOfPushContext()
	{
		static const size_t offset = 12;

		return offset;
	}
	
	const size_t PreAlternative::GetOffsetOfPostAlternative()
	{
		static const size_t offset = 19;

		return offset;
	}
	
	const size_t PreAlternative::GetCodeSize()
	{
		return sizeof( kCode_ );
	}

	__declspec( naked ) void __stdcall Check()
	{
		uint32_t depth;
		bool skip;
		bool force;

		__asm
		{
			pushfd
			pushad
			push	ebp
			mov		ebp, esp
			sub		esp, __LOCAL_SIZE

			mov		al, [ebp+48]			// get force flag
			mov		[force], al
		}

		skip = Merak::tls::IsDisabledSlot();

		if ( skip )
		{
			__asm
			{
				mov		eax, [ebp+44]		// get alternativeProlog
				mov		[ebp+40], eax		// overwrite return address
				jmp		epilog
			}
		}

		depth = Merak::tls::GetDepthOfCall();

		if ( 0 < depth && !force )
		{
			__asm
			{
				mov		eax, [ebp+44]		// get alternativeProlog
				mov		[ebp+40], eax		// overwrite return address
				jmp		epilog
			}
		}
		
		__asm
		{
		epilog:
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			ret		8
		}
	}

	__declspec( naked ) void __stdcall PreStub()
	{
		bool skip;
		bool force;

		__asm
		{
			pushfd
			pushad
			push	ebp
			mov		ebp, esp
			sub		esp, __LOCAL_SIZE

			mov		al, [ebp+48]			// get force flag
			mov		[force], al
		}

		skip = Merak::tls::IsDisabledSlot();

		if ( skip )
		{
			__asm
			{
				mov		eax, [ebp+44]		// get alternativeProlog
				mov		[ebp+40], eax		// overwrite return address
				jmp		epilog
			}
		}

		__asm
		{
			lea		eax, [ebp+52]			// original return
			push	[eax]
			push	eax
			call	Merak::thread::PushContext

			mov		eax, [PostStub]
			mov		[ebp+52], eax			// overwrite original return
		}

		skip = Merak::tls::IncreaseDepthOfCall();

		if ( force )
		{
			skip = false;
		}

		if ( skip )
		{
			__asm
			{
				mov		eax, [ebp+44]		// get alternativeProlog
				mov		[ebp+40], eax		// overwrite return address
			}
		}

		__asm
		{
		epilog:
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			ret		8
		}
	}

	__declspec( naked ) void __stdcall PostStub()
	{
		uintptr_t originalReturn;

		__asm
		{
			push	eax					// dummy (for store return address)
			pushfd
			pushad

			push	ebp					// allocate auto variables
			mov		ebp, esp
			sub		esp, __LOCAL_SIZE
		}

		Merak::tls::DecreaseDepthOfCall();

		Merak::thread::PopContext( NULL, &originalReturn );

		Merak::tls::LoadLastError();

		__asm
		{
			mov		eax, [originalReturn]
			mov		[ebp+40], eax		// recovery return address

			mov		esp, ebp			// free auto variables
			pop		ebp
			
			popad
			popfd
			ret
		}
	}

	__declspec( naked ) void __stdcall PostAlternative()
	{
		uintptr_t originalReturn;

		__asm
		{
			push	eax					// dummy (for store return address)
			pushfd
			pushad
			push	ebp
			mov		ebp, esp
			sub		esp, __LOCAL_SIZE
		}

		Merak::tls::SaveLastError();

		Merak::thread::PopContext( NULL, &originalReturn );

		__asm
		{
			mov		eax, [originalReturn]
			mov		[ebp+40], eax
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			ret
		}
	}

	__declspec( naked ) void __stdcall EnterHandlerForCDECLWrapper()
	{
		__asm
		{
			//    00 wrapper
			//    04 object
			//    08 original return
			//    12 origninal return
			// -> 16 arg 1

			lea		esp, [esp+16]
			push	[esp-12]			// arg2 is object (for wrapper)

			//    00 wrapper
			//    04 object
			//    08 original return
			// -> 12 object
			//    16 arg 1

			push	[esp-4]				// arg1 is original return (for wrapper)

			//    00 wrapper
			//    04 object
			// -> 08 original return
			//    12 object
			//    16 arg 1

			call	[esp-8]				// call wrapper

			// -> 00 return
			//    04 original return
			//    08 object
			//    12 arg 1

			// ... return

			// -> 00 original return
			//    04 object
			//    08 arg 1

			lea		esp, [esp+8]

			//    00 original return
			//    04 object
			// -> 08 arg 1

			push	[esp-8]

			//    00 original return
			// -> 04 original return
			//    08 arg 1

			ret							// return to oringinal

			//    00 
			//    04 
			// -> 08 arg 1
		}
	}

	__declspec( naked ) void __stdcall EnterHandlerForPre()
	{
		__asm
		{
			pushfd
			pushad
			push	ebp
			mov		ebp, esp
			cld
			push	[ebp+40]			// arg 1 is object
			call	[ebp+44]			// call preFunction
			test	al, al
			jnz		epilog
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			lea		esp, [esp+20]		// delete this function's args
			jmp		dword ptr [esp-4]	// jmp toExit
		epilog:
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			lea		esp, [esp+20]		// delete args
			jmp		dword ptr [esp-12]	// jmp alternativeProlog
		}
	}

	__declspec( naked ) void __stdcall EnterHandlerForPrePost()
	{
		__asm
		{
			pushfd
			pushad
			push	ebp
			mov		ebp, esp
			cld
			lea		eax, [ebp+60]		// get original return
			push	eax
			call	Merak::tls::SaveArguments
			lea		eax, [ebp+60]		// get original return
			push	[eax]
			push	eax
			call	Merak::thread::PushContext
			push	[ebp+40]			// arg 1 is object
			call	[ebp+44]			// call preFunction
			test	al, al
			jnz		epilog
			push	0
			push	0
			call	Merak::thread::PopContext
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			lea		esp, [esp+20]		// delete this function's args
			jmp		dword ptr [esp-4]	// jmp toExit
		epilog:
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			lea		esp, [esp+24]		// delete args + ret address
			push	[esp-12]			// overwrite return address
			jmp		dword ptr [esp-12]	// jmp alternativeProlog
		}
	}

	__declspec( naked ) void __stdcall LeaveHandlerForPrePost()
	{
		__asm
		{
			pushfd
			pushad
			push	ebp
			mov		ebp, esp
			push	[ebp+40]			// arg 2 is object
			push	eax					// arg 1 is return value
			call	[ebp+44]			// call postFunction
			call	Merak::tls::ClearArguments
			push	esp
			lea		eax, [esp]
			push	eax
			push	0
			call	Merak::thread::PopContext
			mov		eax, [esp]
			mov		[ebp+44], eax		// recovery return address
			mov		esp, ebp
			pop		ebp
			popad
			popfd
			lea		esp, [esp+4]
			ret
		}
	}

#pragma optimize( "", on )
#pragma runtime_checks( "", restore )
#pragma check_stack( on )

} } // namespace Merak::stub
