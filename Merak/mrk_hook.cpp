﻿#include <algorithm>
#include "mrk_hook_impl.h"
#include "mrk_memory.h"
#include "mrk_stub.h"
#include "mrk_instruction.h"

namespace Merak { namespace hook
{
	void * GetAlternativeFunction( __in const HookObject * object )
	{
		if ( NULL == object || NULL == object->prolog )
		{
			return NULL;
		}

		return impl::GetPreAlternativeProlog( object->prolog );
	}

	PrologLength GetPrologLength(
		__in const void * targetFunction,
		__in size_t replacement )
	{
		return impl::AlternativeProlog( targetFunction ).getLength( replacement );
	}

	HookObject * CreateNativeWrapperHook(
		__in CC::type cc,
		__in const void * targetFunction,
		__in const void * wrapperFunction,
		__in const PrologLength & prologLength,
		__in bool force /* = false */ )
	{
		using namespace Merak::helper;
		using namespace Merak::wrapper;
		using namespace Merak::stub;

		// HookObject 生成
		HookObject * object = impl::CreateHookObject( targetFunction, prologLength, 2, 40 );

		if ( NULL == object )
		{
			return NULL;
		}

		uintptr_t * relayTable = impl::GetRelayTable( object );
		uint8_t * bridgeCode = impl::GetBridgeCode( object );

		if ( !impl::AlternativeProlog( targetFunction ).build( object->prolog ) )
		{
			DeleteHookObject( object );

			return NULL;
		}

		uint8_t * alternativeProlog = impl::GetPreAlternativeProlog( object->prolog );

		// backup
		impl::BackupOriginalProlog( targetFunction, object );

		uint32_t pc = 0;

		// 68h/0xXXXXXXXX: push force flag
		Merak::instruction::Push( &bridgeCode[pc], force, pc );
		// 68h/0xXXXXXXXX: push alternativeProlog
		Merak::instruction::Push( &bridgeCode[pc], alternativeProlog, pc );
		// FF15h/0xXXXXXXXX -> target: call [relayTable] -> Merak::stub::Check
		Merak::instruction::AbsCall( &bridgeCode[pc], Check, &relayTable[0], pc );
		// FFh/34h/24h: push dword ptr [esp] -- new 1st argument
		Merak::instruction::PushEsp( &bridgeCode[pc], 0x00, pc );

		switch ( cc )
		{
		case CC::__STDCALL:
			// C7h/44h/24h/04h/0xXXXXXXXX: mov dword ptr [esp+4], object -- new 2nd argument
			Merak::instruction::MovToEsp( &bridgeCode[pc], object, 0x04, pc );
			// FFh/34h/24h: push dword ptr [esp] (copy return address down two position)
			Merak::instruction::PushEsp( &bridgeCode[pc], 0x00, pc );
			// FF25h/0xXXXXXXXX -> target: jmp [relayTable] -> wrapperFunction
			Merak::instruction::AbsJmp( &bridgeCode[pc], wrapperFunction, &relayTable[1], pc );

			break;
		case CC::__CDECL:
			// 68h/0xXXXXXXXX: push object
			Merak::instruction::Push( &bridgeCode[pc], object, pc );
			// 68h/0xXXXXXXXX: push wrapperFunction
			Merak::instruction::Push( &bridgeCode[pc], wrapperFunction, pc );
			// FF25h/0xXXXXXXXX -> target: jmp [relayTable] -> Merak::stub::EnterHandlerForCDECLWrapper
			Merak::instruction::AbsJmp( &bridgeCode[pc], EnterHandlerForCDECLWrapper, &relayTable[1], pc );
			
			break;
		}

		// Flush
		Merak::instruction::Flush( impl::GetBridgeCode( object ), object->bridge.length );

		return object;
	}

	HookObject * CreateWrapperHook(
		__in CC::type cc,
		__in const void * targetFunction,
		__in const void * wrapperFunction,
		__in const PrologLength & prologLength,
		__in bool force /* = false */ )
	{
		using namespace Merak::helper;
		using namespace Merak::wrapper;
		using namespace Merak::stub;

		// HookObject 生成
		HookObject * object = impl::CreateHookObject( targetFunction, prologLength, 2, 40 );

		if ( NULL == object )
		{
			return NULL;
		}

		uintptr_t * relayTable = impl::GetRelayTable( object );
		uint8_t * bridgeCode = impl::GetBridgeCode( object );

		if ( !impl::AlternativeProlog( targetFunction ).build( object->prolog ) )
		{
			DeleteHookObject( object );

			return NULL;
		}

		uint8_t * alternativeProlog = impl::GetPreAlternativeProlog( object->prolog );

		// backup
		impl::BackupOriginalProlog( targetFunction, object );

		uint32_t pc = 0;

		// 68h/0xXXXXXXXX: push force flag
		Merak::instruction::Push( &bridgeCode[pc], force, pc );
		// 68h/0xXXXXXXXX: push alternativeProlog
		Merak::instruction::Push( &bridgeCode[pc], alternativeProlog, pc );
		// FF15h/0xXXXXXXXX -> target: call [relayTable] -> Merak::stub::PreStub
		Merak::instruction::AbsCall( &bridgeCode[pc], PreStub, &relayTable[0], pc );
		// FFh/34h/24h: push dword ptr [esp] (move return address down one position)
		Merak::instruction::PushEsp( &bridgeCode[pc], 0x00, pc );
		
		switch ( cc )
		{
		case CC::__STDCALL:
			// C7h/44h/24h/04h/0xXXXXXXXX: mov dword ptr [esp+4], object -- new 1st argument
			Merak::instruction::MovToEsp( &bridgeCode[pc], object, 0x04, pc );
			// FF25h/0xXXXXXXXX -> target: jmp [relayTable] -> wrapperFunction
			Merak::instruction::AbsJmp( &bridgeCode[pc], wrapperFunction, &relayTable[1], pc );
			
			break;
		case CC::__CDECL:
			// 68h/0xXXXXXXXX: push object
			Merak::instruction::Push( &bridgeCode[pc], object, pc );
			// 68h/0xXXXXXXXX: push wrapperFunction
			Merak::instruction::Push( &bridgeCode[pc], wrapperFunction, pc );
			// FF25h/0xXXXXXXXX -> target: jmp [relayTable] -> Merak::stub::EnterHandlerForCDECLWrapper
			Merak::instruction::AbsJmp( &bridgeCode[pc], EnterHandlerForCDECLWrapper, &relayTable[1], pc );

			break;
		}		
		
		// Flush
		Merak::instruction::Flush( impl::GetBridgeCode( object ), object->bridge.length );

		return object;
	}

	HookObject * CreatePrePostHook(
		__in const void * targetFunction,
		__in const void * preFunction,
		__in const void * postFunction,
		__in const PrologLength & prologLength,
		__in bool force, /* = false */
		__in uint16_t retArgSize /* = 0 */ )
	{
		using namespace Merak::helper;
		using namespace Merak::wrapper;
		using namespace Merak::stub;

		// HookObject 生成
		HookObject * object = impl::CreateHookObject( targetFunction, prologLength, 3, 72 );

		if ( NULL == object )
		{
			return NULL;
		}

		uintptr_t * relayTable = impl::GetRelayTable( object );
		uint8_t * bridgeCode = impl::GetBridgeCode( object );
		
		if ( !impl::AlternativeProlog( targetFunction ).build( object->prolog ) )
		{
			DeleteHookObject( object );

			return NULL;
		}

		uint8_t * alternativeProlog = impl::GetPreAlternativeProlog( object->prolog );

		// backup
		impl::BackupOriginalProlog( targetFunction, object );

		uint32_t pc = 0;
		uintptr_t * toExit = NULL;
		uintptr_t * tempReturn = NULL;

		// 68h/0xXXXXXXXX: push force flag
		Merak::instruction::Push( &bridgeCode[pc], force, pc );
		// 68h/0xXXXXXXXX: push alternativeProlog
		Merak::instruction::Push( &bridgeCode[pc], alternativeProlog, pc );
		// FF15h/0xXXXXXXXX -> target: call [relayTable] -> Merak::stub::PreStub
		Merak::instruction::AbsCall( &bridgeCode[pc], PreStub, &relayTable[0], pc );
		
		// 68h/0xXXXXXXXX: push toExit
		toExit = Merak::instruction::Push( &bridgeCode[pc], 0x00000000, pc );

		if ( postFunction )
		{
			// 68h/0xXXXXXXXX: push @1
			tempReturn = Merak::instruction::Push( &bridgeCode[pc], 0x00000000, pc );
		}
		else
		{
			// FFh/74h/24h/04h: push [esp+4]
			Merak::instruction::PushEsp( &bridgeCode[pc], 0x04, pc );
		}

		// 68h/0xXXXXXXXX: push alternativeProlog
		Merak::instruction::Push( &bridgeCode[pc], alternativeProlog, pc );
		// 68h/0xXXXXXXXX: push preFunction
		Merak::instruction::Push( &bridgeCode[pc], preFunction, pc );
		// 68h/0xXXXXXXXX: push object
		Merak::instruction::Push( &bridgeCode[pc], object, pc );

		// FF25h/0xXXXXXXXX -> target: jmp [relayTable] -> Merak::stub::EnterHandlerForPrePost
		//												   or Merak::stub::EnterHandlerForPre
		if ( postFunction )
		{
			Merak::instruction::AbsJmp( &bridgeCode[pc], EnterHandlerForPrePost, &relayTable[1], pc );
		}
		else
		{
			Merak::instruction::AbsJmp( &bridgeCode[pc], EnterHandlerForPre, &relayTable[1], pc );
		}				

		if ( tempReturn )
		{
			*tempReturn = reinterpret_cast< uintptr_t >( &bridgeCode[pc] );
		}
		
		// 68h/0xXXXXXXXX: push postFunction --> @1
		Merak::instruction::Push( &bridgeCode[pc], postFunction, pc );
		// 68h/0xXXXXXXXX: push object
		Merak::instruction::Push( &bridgeCode[pc], object, pc );
		// FF25h/0xXXXXXXXX -> target: jmp [relayTable] -> Merak::stub::LeaveHandlerForPrePost
		Merak::instruction::AbsJmp(
			&bridgeCode[pc], 
			reinterpret_cast< uintptr_t >( LeaveHandlerForPrePost ), 
			&relayTable[2], 
			pc );

		// toExit
		*toExit = reinterpret_cast< uintptr_t >( &bridgeCode[pc] );

		// C2h/0xXXXX: ret retArgSize
		Merak::instruction::Ret( &bridgeCode[pc], retArgSize, pc );
		// Flush
		Merak::instruction::Flush( impl::GetBridgeCode( object ), object->bridge.length );

		return object;
	}

	void DeleteHookObject( __in HookObject * object )
	{
		if ( object->prolog )
		{
			Merak::memory::ReleasePool( object->prolog );
		}

		Merak::memory::ReleasePool( object );
	}

	bool ReplaceProlog( __in HookObject * object )
	{
		using namespace Merak::wrapper;
		using namespace Merak::helper;

		uint32_t * target32 = reinterpret_cast< uint32_t * >( object->targetFunction );
		uint16_t * target16 = reinterpret_cast< uint16_t * >( object->targetFunction );
		uint8_t * target8 = reinterpret_cast< uint8_t * >( object->targetFunction );

		Merak::instruction::Protect protect;

		if ( !protect.execute( object->targetFunction, object->prolog->length.replacement, PAGE_EXECUTE_READWRITE ) )
		{
			return false;
		}

		if ( !impl::SelectAttachStyle( object ) )
		{
			return false;
		}

		switch ( object->attachStyle )
		{
		case AttachStyle::RELJMP:
			{
				CodeBytes targetEntry;
				targetEntry.bytes[0x00] = 0xEB;		// jmp $
				targetEntry.bytes[0x01] = 0xFE;
				targetEntry.bytes[0x02] = target8[0x02];
				targetEntry.bytes[0x03] = target8[0x03];

				*target32 = targetEntry.value;

				RelJmp jmp = 
				{
					0xE9, 
					SubAddress( impl::GetBridgeCode( object ), AddAddress( object->targetFunction, sizeof( RelJmp ) ) )
				};

				targetEntry.bytes[0x00] = jmp.opcode;
				targetEntry.bytes[0x01] = jmp.bytes[0x00];
				targetEntry.bytes[0x02] = jmp.bytes[0x01];
				targetEntry.bytes[0x03] = jmp.bytes[0x02];

				target8[0x04] = jmp.bytes[0x03];

				*target32 = targetEntry.value;
			}
			break;
		case AttachStyle::HOTPATCH_NOP:
		case AttachStyle::HOTPATCH_INT3:
			{
				// hot-pattch attach style

				RelJmp jmp = 
				{
					0xE9, 
					SubAddress( impl::GetBridgeCode( object ), AddAddress( object->targetFunction, 0 ) )
				};

				void * previnst = target8 - sizeof( jmp );

				Merak::instruction::Protect prevprotect;

				if ( !prevprotect.execute( previnst, sizeof( jmp ), PAGE_EXECUTE_READWRITE ) )
				{
					return false;
				}

				Copy( target8 - sizeof( jmp ), &jmp, sizeof( jmp ) );

				*target16 = 0xF9EB;					// jmp -5
			}
			break;
		};

		return true;
	}

	bool RestoreProlog( __in const HookObject * object )
	{
		using namespace Merak::wrapper;
		using namespace Merak::helper;
		
		uint32_t * target32 = reinterpret_cast< uint32_t * >( object->targetFunction );
		uint16_t * target16 = reinterpret_cast< uint16_t * >( object->targetFunction );
		uint8_t * target8 = reinterpret_cast< uint8_t * >( object->targetFunction );
		uint8_t * orgProlog = impl::GetOriginalProlog( object->prolog );

		Merak::instruction::Protect protect;

		if ( !protect.execute( object->targetFunction, object->prolog->length.replacement, PAGE_EXECUTE_READWRITE ) )
		{
			return false;
		}

		CodeBytes targetEntry;

		switch ( object->attachStyle )
		{
		case AttachStyle::RELJMP:
			{
				targetEntry.bytes[0x00] = 0xEB;		// jmp $
				targetEntry.bytes[0x01] = 0xFE;
				targetEntry.bytes[0x02] = target8[0x02];
				targetEntry.bytes[0x03] = target8[0x03];

				*target32 = targetEntry.value;

				targetEntry.bytes[0x00] = orgProlog[0x00];
				targetEntry.bytes[0x01] = orgProlog[0x01];
				targetEntry.bytes[0x02] = orgProlog[0x02];
				targetEntry.bytes[0x03] = orgProlog[0x03];
				
				target8[0x04] = orgProlog[0x04];

				*target32 = targetEntry.value;
			}
			break;
		case AttachStyle::HOTPATCH_NOP:
		case AttachStyle::HOTPATCH_INT3:
			{
				// hot-pattch attach style

				*target16 = *reinterpret_cast< uint16_t * >( orgProlog );

				void * previnst = target8 - sizeof( RelJmp );

				Merak::instruction::Protect prevprotect;

				if ( !prevprotect.execute( previnst, sizeof( RelJmp ), PAGE_EXECUTE_READWRITE ) )
				{
					return false;
				}

				if ( AttachStyle::HOTPATCH_NOP == object->attachStyle )
				{
					Merak::helper::Fill( previnst, 0x90, sizeof( RelJmp ) );
				}
				else
				{
					Merak::helper::Fill( previnst, 0xCC, sizeof( RelJmp ) );
				}
			}
			break;
		};

		return true;
	}

} } // namespace Merak::hook